package com.zondy.mapgis.explorer;


import java.io.File;

import android.app.Service;
import android.os.Environment;
import android.os.Vibrator;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.zondy.dizhi.server.util.APPNetWork;
import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseApplication;
import com.zondy.mapgis.android.service.LocationService;
import com.zondy.mapgis.android.utils.FileOperation;

/**
 * 整个应用程序Applicaiton
 * 
 * @author FJL
 * @version 1.0
 * 
 */
public class MapApplication extends BaseApplication
{
	/** 手机sdcard路径 **/
	public static final String PHONE_SDCARD_PATH = Environment.getExternalStorageDirectory().getPath();
	/** 系统库路径 **/
	public static String SystemLibraryPath = PHONE_SDCARD_PATH + "/MapGIS Explorer/";
	/** 地图数据路径 **/
	private String MapDataPath = FilePathCfg.GOOGLE_FILE_PATH;
	/**百度定位服务**/
	public LocationService locationService;
    public Vibrator mVibrator;
    /**TracksRecordSettingFrag中定位参数是否被设置**/
    public boolean IsRecodSetting = false;
	/** 地图Activity **/
	private MapActivity mapActivity;
	/** 地图Application **/
	private static MapApplication app;
	private Class<? extends BaseMapActivity> mapActivityClass;

	@Override
	public void onCreate()
	{
		super.onCreate();
		app = this;
		
		//初始化离线数据及地图符号库字体库
//		initOffineData();
//		String fileFolder = MapApplication.SystemLibraryPath + "Map";
//		File file = new File(fileFolder);
//		if(!file.exists() && !file.isDirectory())
//		{
//			FileOperation.mapFromAsset(getApplicationContext(), "MapGIS Explorer.zip");
//		}
//		com.zondy.mapgis.android.environment.Environment.initialize(MapApplication.SystemLibraryPath, getApplicationContext());
//		com.zondy.mapgis.android.environment.Environment.setRootPath(SystemLibraryPath);
//		MapInitializer.initialize(this);
		/***
         * 初始化定位sdk，建议在Application中创建
         */
        locationService = new LocationService(getApplicationContext());
        mVibrator =(Vibrator)getApplicationContext().getSystemService(Service.VIBRATOR_SERVICE);
        
        if(APPNetWork.isNetWork(this))
        {
        	UMConfigure.init(this, "58e44a1c677baa75d0001bc1", "Umeng", UMConfigure.DEVICE_TYPE_PHONE, "null");
        }
	}
	
	 //各个平台的配置，建议放在全局Application或者程序入口
    {
        //微信 wx12342956d1cab4f9,a5ae111de7d9ea137e88a5e02c07c94d
    	PlatformConfig.setWeixin("wx3729185452e3a275", "49d17efdbc603780e6001b658f6fa9fd");
        //新浪微博
    	PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad", "http://sns.whalecloud.com");
    	PlatformConfig.setYixin("yxc0614e80c9304c11b0391514d09f13bf");
//        PlatformConfig.setSinaWeibo("3921700954", "04b48b094faeb16683c32669824ebdad");
        /*最新的版本需要加上这个回调地址，可以在微博开放平台申请的应用获取，必须要有*/
//        Config.REDIRECT_URL="http://sns.whalecloud.com/sina2/callback";
       //QQ
    	PlatformConfig.setQQZone("100424468", "c7394704798a158208a74ab60104f0ba");

    }
	
	/**
	 * 初始化离线数据
	 */
	public void initOffineData()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				String fileFolder = MapApplication.SystemLibraryPath + "Map";
				File file = new File(fileFolder);
				if(!file.exists() && !file.isDirectory())
				{
					FileOperation.mapFromAsset(getApplicationContext(), "MapGIS Explorer.zip");
					com.zondy.mapgis.android.environment.Environment.initialize(SystemLibraryPath, app);
				}
			}
		}).start();
	}

	public static MapApplication getApp()
	{
		return app;
	}

	public static void setApp(MapApplication app)
	{
		MapApplication.app = app;
	}

	public MapActivity getMapActivity()
	{
		return mapActivity;
	}

	public void setMapActivity(MapActivity mapActivity)
	{
		this.mapActivity = mapActivity;
	}

	public String getSystemLibraryPath()
	{
		return SystemLibraryPath;
	}

	public void setSystemLibraryPath(String systemLibraryPath)
	{
		SystemLibraryPath = systemLibraryPath;
	}

	public String getMapDataPath()
	{
		return MapDataPath;
	}

	public void setMapDataPath(String mapDataPath)
	{
		MapDataPath = mapDataPath;
	}

	public Class<? extends BaseMapActivity> getMapActivityClass()
	{
		return mapActivityClass;
	}

	public void setMapActivityClass(Class<? extends BaseMapActivity> mapActivityClass)
	{
		this.mapActivityClass = mapActivityClass;
	}
	
	public boolean isIsRecodSetting()
	{
		return IsRecodSetting;
	}

	public void setIsRecodSetting(boolean isRecodSetting)
	{
		IsRecodSetting = isRecodSetting;
	}


}
