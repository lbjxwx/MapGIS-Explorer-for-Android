package com.zondy.mapgis.explorer;

import java.io.File;

import android.os.Environment;

/**
 * 相关数据的存放路径
 * @content <修改说明> 请输入修改内容
 * @author admin 2016-3-30 下午4:35:07
 */
public final class FilePathCfg {
	/**
	 * 手机sdcard路径
	 */
	public static final String PHONE_SDCARD_PATH = Environment.getExternalStorageDirectory().getPath();
	/**
	 * 检验文件路径
	 * @param path
	 */
	public static boolean checkPath(String path){
		File file = new File(path);
		if(!file.exists()){
			return false;
		}
		return true;
	}
	/**
	 * 存放地图数据路径
	 * 说明：
	 *      离线矢量数据通过Mapgis 10桌面工具制作并转换移动端离线数据
	 *      离线瓦片可以通过MapGIS 10裁剪的瓦片可直接放到SDCARD上读取
	 *      在线瓦片和在线矢量数据可以由IGServer发布的地图服务服务，移动端可以代码的方式添加在线服务也可以配置地图文件直接loadFromFile
	 *      同时移动端支持对接高德、GOOGLE、百度、天地图等在线服务
	 */
	public static final String MAPX_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/wuhan/wuhan.xml";
	public static final String MAPX_CHINA_PATH = PHONE_SDCARD_PATH + "/MapGIS Explorer/Map/china_temperature/china_temperature.mapx";
	public static final String LIB_FILE_PATH = PHONE_SDCARD_PATH + "/SmartMobile";
	/**
	 * 离线瓦片数据目录
	 * 
	 */
	public static final String TILE_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/tile/world.mapx";
	/**
	 * 存放导航数据文件路径
	 */
	public static final String NAVI_OUTDOORDB_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/wuhan/wuhanNavi/WuhanNaviTest.db";
	/**
	 * 离线POI数据
	 */
	public static final String POI_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/wuhanpoi/wuhanpoi.db"; 
	/**
	 * 离线三维地图数据（灰模）
	 */
	public static final String BUIDING_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/buildings/buildings.mapx";
	/**
	 * 在线服务
	 */
	public static final String GOOGLE_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/google/google.xml";
	public static final String TIANDITU_FILE_PATH = PHONE_SDCARD_PATH + "/MapGIS/Map/google/tianditu.mapx";
}
