package com.zondy.mapgis.explorer;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseActivity;
import com.zondy.mapgis.android.map.dao.ICallBack;
import com.zondy.mapgis.android.utils.FileOperation;
import com.zondy.mapgis.explorer.R;

/**
 * 程序启动界面
 * 
 * @author fjl
 * @version 1.0
 * 
 */
public class Launcher extends BaseActivity
{

	/** 当前Activity渲染的视图View **/
	private View mContextView = null;
	private boolean IsInitOffineData = false;
	private AlphaAnimation mAnimation;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 设置渲染视图View
		mContextView = LayoutInflater.from(this).inflate(R.layout.activity_launcher, null);
		setContentView(mContextView);

		mContext = this;

		// 初始化离线数据及地图符号库字体库
		initOffineData(new ICallBack()
		{
			@Override
			public void onCall(String str)
			{
				com.zondy.mapgis.android.environment.Environment.initialize(MapApplication.SystemLibraryPath, getApplicationContext());
				((Activity) mContext).runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						initView();
					}
				});
			}
		});

//		initView();
	}

	/**
	 * 初始化窗体
	 */
	public void initView()
	{
		setContentView(mContextView);
		// 添加动画效果
		mAnimation = new AlphaAnimation(0.3f, 1.0f);
		mAnimation.setDuration(1500);
		mAnimation.setAnimationListener(new AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{

			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{

			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				// 跳转界面
 				startActivity(new Intent(getApplicationContext(), MapActivity.class));
				// startActivity(new Intent(getApplicationContext(),
				// LoginActivity.class));
				finish();
				// 右往左推出效果
				// overridePendingTransition(R.anim.push_left_in,
				// R.anim.push_left_out);
				// Android内置的
				overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
			}
		});

		mContextView.setAnimation(mAnimation);

	}

	/**
	 * 初始化离线数据
	 */
	public void initOffineData(final ICallBack callback)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				String fileFolder = MapApplication.SystemLibraryPath + "Map/武汉光谷";
				File file = new File(fileFolder);
				if (!file.exists() && !file.isDirectory())
				{
					FileOperation.mapFromAsset(getApplicationContext(), "MapGIS Explorer.zip");
				}
				callback.onCall(null);
			}
		}).start();
	}

	@Override
	public void resume()
	{

	}

	@Override
	public void destroy()
	{

	}

}