package com.zondy.mapgis.map.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.zondy.mapgis.android.utils.StringUtils;
import com.zondy.mapgis.core.attr.FieldType;
import com.zondy.mapgis.map.model.LandFieldType;

/**
 * 查询模版
 * @author admin
 * @date 2014-9-1
 */
public class QueryModel implements Serializable{

	private String layer;
	private final List<String> fldNames;
	private final List<LandFieldType> fldTypes;
	private final List<String> fldRelations;

	public QueryModel() {
		fldNames = new ArrayList<String>();
		fldTypes = new ArrayList<LandFieldType>();
		fldRelations = new ArrayList<String>();
	}

	/**
	 * 取查询图层
	 * @return
	 */
	public String getLayer() {
		return layer;
	}

	/**
	 * 设置查询图层
	 * @param layer
	 */
	public void setLayer(String layer) {
		this.layer = layer;
	}

	/**
	 * 添加属性查询条件
	 * @param fldName 字段名
	 * @param fldValue 字段值
	 * @param fldType 字段类型
	 * @return
	 */
	public QueryModel add(String fldName, String fldRelation, FieldType fldType) {
		fldNames.add(fldName);
		fldRelations.add(fldRelation);
		fldTypes.add(LandFieldType.fromFieldType(fldType));
		return this;
	}
	
	/**
	 * 添加属性查询条件
	 * @param fldName 字段名
	 * @param fldValue 字段值
	 * @param fldType 字段类型
	 * @return
	 */
	public QueryModel add(String fldName, String fldRelation, LandFieldType fldType) {
		fldNames.add(fldName);
		fldRelations.add(fldRelation);
		fldTypes.add(fldType);
		return this;
	}
	
	/**
	 * 添加属性查询条件
	 * @param fldName 字段名
	 * @param fldValue 字段值
	 * @param fldType 字段类型
	 * @return
	 */
	public QueryModel add(String fldName, String fldRelation, int fldType) {
		fldNames.add(fldName);
		fldRelations.add(fldRelation);
//		fldTypes.add(LandFieldType.fromArcFieldType(fldType));
		return this;
	}
	

	/**
	 * 移除一个条件，如果查询条件中没有指定字段，则无操作。
	 * @param fldName 查询字段名
	 * @return 移除后的查询条件字段数
	 */
	public int del(String fldName) {

		int index = StringUtils.getIndex(fldNames, fldName);
		if (index >= 0) {
			fldNames.remove(index);
			fldRelations.remove(index);
			fldTypes.remove(index);
		}
		return fldNames.size();
	}

	/**
	 * 移除一个查询条件
	 * @param index
	 */
	public void del(int index) {
		if (index >= 0 && index < fldNames.size()) {
			fldNames.remove(index);
			fldRelations.remove(index);
			fldTypes.remove(index);
		}
	}

	/**
	 * 查询条件数
	 * @return
	 */
	public int size() {
		return fldNames.size();
	}

	/**
	 * 查询模版是否合法
	 * @return
	 */
	public boolean isLegal() {
		return fldNames.size() > 0 && fldNames.size() == fldRelations.size()
					&& fldNames.size() == fldTypes.size() && layer != null && layer.length() > 0;
	}

	public void clear() {
		fldNames.clear();
		fldRelations.clear();
		fldTypes.clear();
	}

	/**
	 * @return the fldNames
	 */
	protected List<String> getFldNames() {
		return fldNames;
	}

	/**
	 * @return the fldTypes
	 */
	protected List<LandFieldType> getFldTypes() {
		return fldTypes;
	}

	/**
	 * @return the fldRelations
	 */
	protected List<String> getFldRelations() {
		return fldRelations;
	}

	/**
	 * 转换为Json对象
	 * @return
	 */
	public JSONObject toJson() {

		if (!isLegal()) {
//			LogTool.w("QueryModel.toJson", "查询模版不合法，无法转换为Json对象");
			return null;
		}

		JSONObject jsObj = new JSONObject();
		try {
			jsObj.put("layer", layer != null ? layer : "");

			JSONArray jsArrFldName = new JSONArray();
			for (String name : fldNames) {
				jsArrFldName.put(name);
			}
			jsObj.put("fldNames", jsArrFldName);

			JSONArray jsArrFldType = new JSONArray();
			for (LandFieldType fldType : fldTypes) {
				jsArrFldType.put(fldType.toString());
			}
			jsObj.put("fldTypes", jsArrFldType);

			JSONArray jsArrFldRela = new JSONArray();
			for (String rela : fldRelations) {
				jsArrFldRela.put(rela);
			}
			jsObj.put("fldRelations", jsArrFldRela);

			return jsObj;

		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 根据Json对象解析成查询模版
	 * @param jsObj
	 * @return
	 */
	public static QueryModel fromJson(JSONObject jsObj) {

		if (jsObj == null) {
//			LogTool.w("QueryModel.fromJson", "参数jsObj为空，无法解析成查询模版");
			return null;
		}
		QueryModel qm = new QueryModel();
		qm.setLayer(jsObj.optString("layer"));

		JSONArray jsArrFldNames = jsObj.optJSONArray("fldNames");
		JSONArray jsArrFldTypes = jsObj.optJSONArray("fldTypes");
		JSONArray jsArrFldRelas = jsObj.optJSONArray("fldRelations");

		boolean isLegal = jsArrFldNames != null && jsArrFldRelas != null && jsArrFldTypes != null
					&& jsArrFldNames.length() == jsArrFldRelas.length()
					&& jsArrFldNames.length() == jsArrFldTypes.length();

		if (!isLegal) {
			return null;
		}
		int count = jsArrFldNames.length();
		
		for (int index = 0; index < count; index++) {
			
			String strFldName = jsArrFldNames.optString(index);
			String strFldType = jsArrFldTypes.optString(index);
			String strFldRela = jsArrFldRelas.optString(index);
			
			if(strFldName.length() == 0 || strFldType.length() == 0 && strFldRela.length() == 0){
				continue;
			}
			// 2014.11.7 yangsheng 使用新版本，也需要兼容早期版本。
			LandFieldType lft = LandFieldType.fromStringEn(strFldType);
			if(lft == null){
				lft = LandFieldType.fromFieldType(new FieldType(FieldType.getValueByName(FieldType.class, strFldType)));
			}
			
			qm.add(strFldName, strFldRela, lft);
		}
		
		return qm;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryModel [layer=" + layer + ", fldNames=" + fldNames + ", fldTypes=" + fldTypes
					+ ", fldRelations=" + fldRelations + "]";
	}

}
