package com.zondy.mapgis.map.model;

public class GPS
{
	private double wgLat;
	private double wgLon;

	public GPS(double bd_lat, double bd_lon)
	{
		this.wgLat = bd_lat;
		this.wgLon = bd_lon;
	}
	

	public double getWgLat()
	{
		return wgLat;
	}

	public void setWgLat(double wgLat)
	{
		this.wgLat = wgLat;
	}

	public double getWgLon()
	{
		return wgLon;
	}

	public void setWgLon(double wgLon)
	{
		this.wgLon = wgLon;
	}

	@Override
	public String toString()
	{
		return wgLat + "," + wgLon;
	}
}
