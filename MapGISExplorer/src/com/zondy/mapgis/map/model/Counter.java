package com.zondy.mapgis.map.model;

/**
 * 计数器
 */
public class Counter {
	
	private int count = 0;
	
	public Counter(int count){
		this.count = count;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public void increase(){
		count++;
	}
	public void decrease(){
		count--;
	}
	
}
