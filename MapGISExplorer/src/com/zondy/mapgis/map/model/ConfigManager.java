package com.zondy.mapgis.map.model;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 轨迹记录参数设置类
 * 
 * @author fjl
 * 
 */
public class ConfigManager
{
	private static ConfigManager myConfig = null;
	private Context context;
	private String preferenceName = "system_config";
	private String coordType = "";
	private String transStr = "";
	public double dx;
	public double dy;
	public double dz;
	public double wx;
	public double wy;
	public double wz;
	public double dm;

	private String recordMode = null;
	private int recordTime = 1000;

	public static ConfigManager getInstance(Context context)
	{
		if (null == myConfig)
		{
			myConfig = new ConfigManager();
			myConfig.context = context;
			myConfig.readConfig();
		}
		return myConfig;
	}

	/**
	 * 读取配置文件
	 * 
	 * @return
	 */
	public boolean readConfig()
	{
		if (null == context)
			return false;
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		recordTime = preferences.getInt("recordTime", 1000);
		coordType = preferences.getString("coordType", "GCJ02");
		dx = Double.valueOf(preferences.getString("dx", "0"));
		dy = Double.valueOf(preferences.getString("dy", "0"));
		dz = Double.valueOf(preferences.getString("dz", "0"));
		wx = Double.valueOf(preferences.getString("wx", "0"));
		wy = Double.valueOf(preferences.getString("wy", "0"));
		wz = Double.valueOf(preferences.getString("wz", "0"));
		dm = Double.valueOf(preferences.getString("dm", "0"));
		return false;
	}

	/**
	 * 保存配置文件
	 * 
	 * @return
	 */
	public boolean saveConfig()
	{
		if (null == context)
			return false;
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putInt("recordTime", recordTime);
		editor.putString("coordType", myConfig.getCoordType());
		editor.putString("transParams", myConfig.getTransStr());
		editor.putString("dx", String.valueOf(myConfig.getDx()));
		editor.putString("dy", String.valueOf(myConfig.getDy()));
		editor.putString("dz", String.valueOf(myConfig.getDz()));
		editor.putString("wx", String.valueOf(myConfig.getWx()));
		editor.putString("wy", String.valueOf(myConfig.getWy()));
		editor.putString("wz", String.valueOf(myConfig.getWz()));
		editor.putString("dm", String.valueOf(myConfig.getDm()));
		editor.commit();
		return true;
	}

	public String getRecordMode()
	{
		return recordMode;
	}

	public void setRecordMode(String recordMode)
	{
		this.recordMode = recordMode;
	}

	public int getRecordTime()
	{
		return recordTime;
	}

	public void setRecordTime(int recordTime)
	{
		this.recordTime = recordTime;
	}

	public String getCoordType()
	{
		return coordType;
	}

	public void setCoordType(String coordType)
	{
		this.coordType = coordType;
	}

	public String getTransStr()
	{
		return transStr;
	}

	public void setTransStr(String transStr)
	{
		this.transStr = transStr;
	}

	public int type;

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public double getDx()
	{
		return dx;
	}

	public void setDx(double dx)
	{
		this.dx = dx;
	}

	public double getDy()
	{
		return dy;
	}

	public void setDy(double dy)
	{
		this.dy = dy;
	}

	public double getDz()
	{
		return dz;
	}

	public void setDz(double dz)
	{
		this.dz = dz;
	}

	public double getWx()
	{
		return wx;
	}

	public void setWx(double wx)
	{
		this.wx = wx;
	}

	public double getWy()
	{
		return wy;
	}

	public void setWy(double wy)
	{
		this.wy = wy;
	}

	public double getWz()
	{
		return wz;
	}

	public void setWz(double wz)
	{
		this.wz = wz;
	}

	public double getDm()
	{
		return dm;
	}

	public void setDm(double dm)
	{
		this.dm = dm;
	}

}
