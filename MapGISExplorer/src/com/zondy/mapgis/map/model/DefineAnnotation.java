package com.zondy.mapgis.map.model;

import android.graphics.Bitmap;

import com.zondy.mapgis.android.annotation.Annotation;
import com.zondy.mapgis.core.geometry.Dot;

public class DefineAnnotation extends Annotation
{
	private static final long serialVersionUID = 1L;
	private int id;
	private int imgResourceId;

	public DefineAnnotation(String title, String description, Dot point, Bitmap image, int id)
	{
		super(title, description, point, image);
		this.id = id;
	}

	
	public int getImgResourceId()
	{
		return imgResourceId;
	}


	public void setImgResourceId(int imgResourceId)
	{
		this.imgResourceId = imgResourceId;
	}


	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
