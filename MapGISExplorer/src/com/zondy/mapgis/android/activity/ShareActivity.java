package com.zondy.mapgis.android.activity;

import com.zondy.mapgis.explorer.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ShareActivity extends Activity implements OnClickListener
{

	private TextView mTile = null;
	private ImageView mTileBack = null; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		initView();
	}
	
	/**
	 * 初始化窗体
	 */
	public void initView()
	{
		setContentView(R.layout.activity_feedback);
		
		mTile = (TextView) findViewById(R.id.title_text);
		mTile.setText("意见收件箱");
		mTileBack = (ImageView) findViewById(R.id.title_back);
		mTileBack.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.title_back:
			finish();
			break;

		default:
			break;
		}
		
	}
}
