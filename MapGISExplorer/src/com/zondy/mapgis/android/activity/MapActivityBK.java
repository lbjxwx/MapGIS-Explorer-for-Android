package com.zondy.mapgis.android.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.zondy.mapgis.android.annotation.Annotation;
import com.zondy.mapgis.android.annotation.AnnotationView;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.frag.LayerManagerFragment;
import com.zondy.mapgis.android.frag.MapManagerModuleFrag;
import com.zondy.mapgis.android.frag.SearchModuleFrag;
import com.zondy.mapgis.android.map.dao.ICallBack;
import com.zondy.mapgis.android.map.dao.ICallBackGoBack;
import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.android.mapview.MapView.MapViewAnnotationListener;
import com.zondy.mapgis.android.mapview.MapView.MapViewStopCurRequestCallback;
import com.zondy.mapgis.android.model.EnumViewPos;
import com.zondy.mapgis.android.utils.DialogUtil;
import com.zondy.mapgis.android.utils.PopupWindowUtil;
import com.zondy.mapgis.android.utils.SharedUtils;
import com.zondy.mapgis.android.utils.StringUtils;
import com.zondy.mapgis.android.utils.SysEnv;
import com.zondy.mapgis.android.utils.ToolToast;
import com.zondy.mapgis.core.map.Map;
import com.zondy.mapgis.core.map.MapServer;
import com.zondy.mapgis.core.map.MapServerType;
import com.zondy.mapgis.core.map.ServerLayer;
import com.zondy.mapgis.core.object.Enumeration;
import com.zondy.mapgis.explorer.BaseMapActivity;
import com.zondy.mapgis.map.model.MapInfo;
import com.zondy.mapgis.explorer.R;

/**
 * 基本的地图Activity<br>
 * 使用了一套基础的可扩展的界面，实现了{@link BaseMapActivity}中所有接口，提供一组公用方法给其他模块调用。<br>
 * 创建时，自动加载一个默认地图或上一次打开的地图。
 * 
 * @author fjl
 */
public class MapActivityBK extends BaseMapActivity implements OnClickListener
{

	private String TAG = this.getClass().getSimpleName();
	public MapView mMapView = null;
	private Map mMap = null;
	private MapInfo mMapInfo = new MapInfo();
	private ServerLayer mServerLayer = new ServerLayer();
	private List<BaseFragment> mfrags = new ArrayList<BaseFragment>();
	/**
	 * 地图数据类型
	 */
	public static final int MAPX = 0;
	public static final int IGSERVER = 1;
	public static final int ONLINEMAP = 2;

	public LinearLayout mTopContent = null;
	public LinearLayout mMapBottomContent = null;
	public RelativeLayout mFullcontent = null;
	public LinearLayout mMapRightContent = null;

	ProgressDialog myDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		initView();
		initMap();
//		MapApplication.getApp().setMapActivity(this);
		// dc = MapApplication.getApp().getDatac();
	}

	@Override
	public void initView()
	{
		setContentView(R.layout.main);

		mTopContent = (LinearLayout) find(R.id.maintop_content);
		mMapRightContent = (LinearLayout) findViewById(R.id.map_right_content);
		mMapBottomContent = (LinearLayout) findViewById(R.id.main_bootom);
		mFullcontent = (RelativeLayout) findViewById(R.id.full_content);

		findViewById(R.id.changemap_iv).setOnClickListener(this);
		findViewById(R.id.check_layer_iv).setOnClickListener(this);
		findViewById(R.id.tools_channel).setOnClickListener(this);
		findViewById(R.id.more_channel).setOnClickListener(this);
		findViewById(R.id.map_search_channel).setOnClickListener(this);
	}

	@Override
	public void initMap()
	{
		if (mMap == null)
		{
			mMap = new Map();
		}
		mMapView = (MapView) findViewById(R.id.map_main);
		mMapView.setZoomControlsEnabled(false);
		mMapView.setShowLogo(false);
		mMapView.setScaleBarPoistion(new PointF(SysEnv.SCREEN_WIDTH / 10, SysEnv.SCREEN_HEIGHT - 120));

		mMapInfo = SharedUtils.getInstance().getMapInfo(getApplicationContext());
		int mapType = mMapInfo.getMapType();
		String igServerAddress = mMapInfo.getIgserverAddress();

		if (mapType == MAPX && StringUtils.isNotEmpty(mMapInfo.getMapxPath()))
		{
			// 加载地图文档mapx
			mMapView.loadFromFile(mMapInfo.getMapxPath());
		}
		else if (mapType == IGSERVER && StringUtils.isNotEmpty(mMapInfo.getIgserverAddress()))
		{
			// 加载IGServer地图服务
			if (igServerAddress.contains("docs"))
			{
//				mServerLayer.setMapServerByType(MapServerType.MapGISIGSServerVector);
				MapServer mapServer = mServerLayer.createMapServerByType(MapServerType.MapGISIGSServerVector);
				mapServer.setName("docs");
				mServerLayer.setMapServer(mapServer);
			}
			if (igServerAddress.contains("tile"))
			{
//				mServerLayer.setMapServerByType(MapServerType.MapGISIGSServerTile);
				MapServer mapServer = mServerLayer.createMapServerByType(MapServerType.MapGISIGSServerTile);
				mapServer.setName("tile");
				mServerLayer.setMapServer(mapServer);
			}
			mServerLayer.setURL(igServerAddress);
			mMap.append(mServerLayer);
			mMapView.setMap(mMap);
			mMapView.refresh();
		}
		else if (mapType == ONLINEMAP && StringUtils.isNotEmpty(mMapInfo.getMapServerType()))
		{
			this.runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
//					DialogUtil.showProgressDlg(MapActivity.this, "正在加载在线地图");
				}
			});
			// 加载第三方在线地图
			Enumeration enumeraiton = MapServerType.parse(MapServerType.class, mMapInfo.getMapServerType());
//			mServerLayer.setMapServerByType((MapServerType) enumeraiton);
			MapServer mapServer = mServerLayer.createMapServerByType((MapServerType) enumeraiton);
			mServerLayer.setMapServer(mapServer);
			mMapView.stopCurRequest(new MapViewStopCurRequestCallback()
			{
				@Override
				public void onDidStopCurRequest()
				{
					mMap.append(mServerLayer);
					// mMapView.setMap(mMap);
					// mMapView.refresh();
					mMapView.forceRefresh();
					DialogUtil.hidenThreadProgressDlg();

				}
			});

		}
		else
		{
			ToolToast.showShort("未找到地图");
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.changemap_iv:
			final MapManagerModuleFrag mapManagerFrag = new MapManagerModuleFrag();
			super.popBackStack();
			addFragment(EnumViewPos.TOP, mapManagerFrag, mapManagerFrag.getName(), null);
			addFragBackListen(mapManagerFrag);
			break;
		case R.id.check_layer_iv:
			LayerManagerFragment layerManagFrag = new LayerManagerFragment();
			super.popBackStack();
			addFragment(EnumViewPos.TOP, layerManagFrag, layerManagFrag.getName(), null);
			addFragBackListen(layerManagFrag);
			break;
		case R.id.tools_channel:
//			new PopupWindowUtil().showToolsPopWindow(this, mMapBottomContent);
			break;
		case R.id.map_search_channel:
			if (mMapInfo.getMapType() != MAPX)
			{
				DialogUtil.createOneButtonDialog(this, "你当前显示的地图不支持查询功能，建议你切换地图", null, null, null, "确定", new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface arg0, int arg1)
					{
						MapManagerModuleFrag mapManagerFrag = new MapManagerModuleFrag();
						addFragment(EnumViewPos.TOP, mapManagerFrag, mapManagerFrag.getName(), null);
					}

				}).show();
			}
			else
			{
				mMapBottomContent.setVisibility(View.GONE);
				SearchModuleFrag searchFrag = new SearchModuleFrag();
				super.popBackStack();
				addFragment(EnumViewPos.FULL, searchFrag, searchFrag.getName(), null);
				addFragBackListen(searchFrag);
			}

			break;
		case R.id.more_channel:
			startActivity(new Intent(getApplicationContext(), AccountActivity.class));
			break;
		default:
			break;
		}

	}

	// 添加Fragment（实现抽象父类接口）
	@Override
	public void addFragment(EnumViewPos enumPos, BaseFragment frag, String strTag, ICallBack callback)
	{
		int parentResId = -1;
		switch (enumPos)
		{
		case TOP:
			parentResId = R.id.maintop_content;
			break;

		case FULL:
			parentResId = R.id.full_content;
			break;
		case BOTTOM:
			// 查询模块
		default:
			break;
		}

		if (parentResId < 1)
		{
			return;
		}

		// for(BaseFragment baseFrag : mfrags)
		// {
		// if(baseFrag.getName() == frag.getName())
		// {
		// continue;
		// }
		// if(baseFrag.getparentViewID() == parentResId)
		// {
		// hideFragment(baseFrag.getName(), null);
		// }
		// }

		if (!frag.isAdded())
		{
			addFragment(parentResId, frag, strTag, callback);
			mfrags.add(frag);
		}
		else if (frag.isHidden())
		{
			showFragment(strTag, callback);
		}

	}

	/**
	 * 添加Fragment返回监听
	 * 
	 * @param frag
	 */
	public void addFragBackListen(final BaseFragment frag)
	{
		this.addGoBackListener(new ICallBackGoBack()
		{
			@Override
			public int onCall(String str)
			{
				Log.d(TAG, "remove:" + frag.getName());
				removeFragment(frag.getName(), null);
				mMapBottomContent.setVisibility(View.VISIBLE);
				mMapRightContent.setVisibility(View.VISIBLE);
				return 1;
			}
		});
	}

	class annotionViewListen implements MapViewAnnotationListener
	{

		@Override
		public void mapViewClickAnnotation(MapView mapView, Annotation annotation)
		{
			// TODO Auto-generated method stub

		}

		@Override
		public boolean mapViewWillShowAnnotationView(MapView mapView, AnnotationView annotationView)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean mapViewWillHideAnnotationView(MapView mapView, AnnotationView annotationView)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public AnnotationView mapViewViewForAnnotation(MapView mapView, Annotation annotation)
		{
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void mapViewClickAnnotationView(MapView mapView, AnnotationView annotationView)
		{
			// TODO Auto-generated method stub

		}

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void destroy()
	{

	}

	@Override
	public boolean goback()
	{
		if (PopupWindowUtil.popupWindow != null)
		{
			PopupWindowUtil.popupWindow.dismiss();
		}

		if (mfrags.size() > 1)
		{
			// super.popBackStack();
		}
		if (mfrags.size() < 1)
		{
			mMapBottomContent.setVisibility(View.VISIBLE);
			return true;
		}
		return false;
	}

}
