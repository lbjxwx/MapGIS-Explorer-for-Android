package com.zondy.mapgis.android.activity;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.zondy.mapgis.explorer.R;

/**
 * 意见反馈Activity
 * 
 * @author fjl
 * @version 1.0
 */
public class FeedbackActivity extends Activity implements OnClickListener
{

	private TextView mTile = null;
	private ImageView mTileBack = null; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		initView();
	}
	
	/**
	 * 初始化窗体
	 */
	public void initView()
	{
		setContentView(R.layout.activity_feedback);
		//透明状态栏  
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);  
        //透明导航栏  
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
		
		mTile = (TextView) findViewById(R.id.title_text);
		mTile.setText("意见收件箱");
		mTileBack = (ImageView) findViewById(R.id.title_back);
		mTileBack.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.title_back:
			finish();
			break;

		default:
			break;
		}
		
	}
}
