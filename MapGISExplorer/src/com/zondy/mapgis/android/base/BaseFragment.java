package com.zondy.mapgis.android.base;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zondy.mapgis.android.map.dao.IBaseFragment;

/**
 * Fragment基类
 * 
 * @author Fjl
 * @version 1.0
 * 
 */
public abstract class BaseFragment extends Fragment implements IBaseFragment
{

	/** 当前Fragment渲染的视图View **/
	private View mContextView = null;
	private int iParentViewID;
	protected Map<String, Object> data = new HashMap<String, Object>();
	/** 日志输出标志 **/
	protected final String TAG = this.getClass().getSimpleName();

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		Log.d(TAG, "BaseFragment-->onAttach()");
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.d(TAG, "BaseFragment-->onCreate()");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d(TAG, "BaseFragment-->onCreateView()");

		// 渲染视图View(防止切换时重绘View)
		if (null != mContextView)
		{
			ViewGroup parent = (ViewGroup) mContextView.getParent();
			if (null != parent)
			{
				parent.removeView(mContextView);
			}
		}
		else
		{
			// mContextView = inflater.inflate(bindLayout(), container);
			mContextView = inflater.inflate(bindLayout(), container, false);
			// 控件初始化
			initView(mContextView);
		}

		// 业务处理
		doBusiness(getActivity());

		return mContextView;
	}

	/*
	 * 获取容器View的ID
	 */
	public int getparentViewID()
	{
		return iParentViewID;
	}

	public void setParentViewID(int parentViewID)
	{
		iParentViewID = parentViewID;
	}

	
	protected Object getArgument(String strName)
	{
		return data.get(strName);
	}

	public void putArgument(String strName, Object objValue)
	{
		data.put(strName, objValue);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		Log.d(TAG, "BaseFragment-->onActivityCreated()");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		Log.d(TAG, "BaseFragment-->onSaveInstanceState()");
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onStart()
	{
		Log.d(TAG, "BaseFragment-->onStart()");
		super.onStart();
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "BaseFragment-->onResume()");
		super.onResume();
	}

	@Override
	public void onPause()
	{
		Log.d(TAG, "BaseFragment-->onPause()");
		super.onPause();
	}

	@Override
	public void onStop()
	{
		Log.d(TAG, "BaseFragment-->onStop()");
		super.onStop();
	}

	@Override
	public void onDestroyView()
	{
		Log.d(TAG, "BaseFragment-->onDestroyView()");
		super.onDestroyView();
		// ((ViewGroup)mContextView.getParent()).removeView(mContextView);
	}

	@Override
	public void onDestroy()
	{
		Log.d(TAG, "BaseFragment-->onDestroy()");
		super.onDestroy();
	}

	@Override
	public void onDetach()
	{
		Log.d(TAG, "BaseFragment-->onDetach()");
		super.onDetach();
	}

	/**
	 * 获取当前Fragment依附在的Activity
	 * 
	 * @return
	 */
	protected Activity getContext()
	{
		return getActivity();
	}
	/**
	 * 移除当前Activity
	 */
	public void close() {
      ((BaseActivity) getActivity()).removeFragment(getName(), null);
  }
}
