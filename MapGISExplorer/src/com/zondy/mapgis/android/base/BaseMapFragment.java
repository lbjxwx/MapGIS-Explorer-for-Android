package com.zondy.mapgis.android.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

/*
 * @author frost
 * @Description *所有要在mapView上以浮动窗口形式显示的Fragment将继承此类
 * 对于在MapActivity上显示的Fragment,我们将其类似于浮动窗口
 * 首先应满足被遮盖的MapView不能被交互,后续可能添加其他特性
 */
public class BaseMapFragment extends BaseFragment{

	@Override
	public int bindLayout()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void initView(View view)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void doBusiness(Context mContext)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override 
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if(view!=null)
		{
			view.setClickable(true);
		}
	}
	public boolean goback() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
}
