package com.zondy.mapgis.android.gpx.extensions;


import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.zondy.mapgis.android.gpx.bean.GPX;
import com.zondy.mapgis.android.gpx.bean.Route;
import com.zondy.mapgis.android.gpx.bean.Track;
import com.zondy.mapgis.android.gpx.bean.Waypoint;

public abstract interface IExtensionParser
{
  public abstract String getId();

  public abstract Object parseWaypointExtension(Node paramNode);

  public abstract Object parseTrackExtension(Node paramNode);

  public abstract Object parseGPXExtension(Node paramNode);

  public abstract Object parseRouteExtension(Node paramNode);

  public abstract void writeGPXExtensionData(Node paramNode, GPX paramGPX, Document paramDocument);

  public abstract void writeWaypointExtensionData(Node paramNode, Waypoint paramWaypoint, Document paramDocument);

  public abstract void writeTrackExtensionData(Node paramNode, Track paramTrack, Document paramDocument);

  public abstract void writeRouteExtensionData(Node paramNode, Route paramRoute, Document paramDocument);
}