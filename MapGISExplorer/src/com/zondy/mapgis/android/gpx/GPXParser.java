package com.zondy.mapgis.android.gpx;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.zondy.mapgis.android.gpx.bean.GPX;
import com.zondy.mapgis.android.gpx.bean.Route;
import com.zondy.mapgis.android.gpx.bean.Track;
import com.zondy.mapgis.android.gpx.bean.Waypoint;
import com.zondy.mapgis.android.gpx.extensions.IExtensionParser;

public class GPXParser
{
  private ArrayList<IExtensionParser> extensionParsers = new ArrayList();
  private Logger logger = Logger.getLogger(super.getClass().getName());

  public void addExtensionParser(IExtensionParser paramIExtensionParser)
  {
    this.extensionParsers.add(paramIExtensionParser);
  }

  public void removeExtensionParser(IExtensionParser paramIExtensionParser)
  {
    this.extensionParsers.remove(paramIExtensionParser);
  }

  public GPX parseGPX(InputStream paramInputStream)
    throws ParserConfigurationException, SAXException, IOException
  {
    DocumentBuilder localDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document localDocument = localDocumentBuilder.parse(paramInputStream);
    Node localNode1 = localDocument.getFirstChild();
    if ((localNode1 != null) && ("gpx".equals(localNode1.getNodeName())))
    {
      GPX localGPX = new GPX();
      NamedNodeMap localNamedNodeMap = localNode1.getAttributes();
      for (int i = 0; i < localNamedNodeMap.getLength(); ++i)
      {
        Node localNode2 = localNamedNodeMap.item(i);
        if ("version".equals(localNode2.getNodeName()))
          localGPX.setVersion(localNode2.getNodeValue());
        else if ("creator".equals(localNode2.getNodeName()))
          localGPX.setCreator(localNode2.getNodeValue());
      }
      NodeList localNodeList = localNode1.getChildNodes();
      this.logger.debug("Found " + localNodeList.getLength() + " child nodes. Start parsing ...");
      for (int j = 0; j < localNodeList.getLength(); ++j)
      {
        Object localObject1;
        Node localNode3 = localNodeList.item(j);
        if ("wpt".equals(localNode3.getNodeName()))
        {
          this.logger.debug("Found waypoint node. Start parsing...");
          localObject1 = parseWaypoint(localNode3);
          if (localObject1 != null)
          {
            this.logger.info("Add waypoint to gpx data. [waypointName=" + ((Waypoint)localObject1).getName() + "]");
            localGPX.addWaypoint((Waypoint)localObject1);
          }
        }
        else if ("trk".equals(localNode3.getNodeName()))
        {
          this.logger.debug("Found track node. Start parsing...");
          localObject1 = parseTrack(localNode3);
          if (localObject1 != null)
          {
            this.logger.info("Add track to gpx data. [trackName=" + ((Track)localObject1).getName() + "]");
            localGPX.addTrack((Track)localObject1);
          }
        }
        else if ("extensions".equals(localNode3.getNodeName()))
        {
          this.logger.debug("Found extensions node. Start parsing...");
          localObject1 = this.extensionParsers.iterator();
          while (((Iterator)localObject1).hasNext())
          {
            IExtensionParser localIExtensionParser = (IExtensionParser)((Iterator)localObject1).next();
            Object localObject2 = localIExtensionParser.parseGPXExtension(localNode3);
            localGPX.addExtensionData(localIExtensionParser.getId(), localObject2);
          }
        }
        else if ("rte".equals(localNode3.getNodeName()))
        {
          this.logger.debug("Found route node. Start parsing...");
          localObject1 = parseRoute(localNode3);
          if (localObject1 != null)
          {
            this.logger.info("Add route to gpx data. [routeName=" + ((Route)localObject1).getName() + "]");
            localGPX.addRoute((Route)localObject1);
          }
        }
      }
      return localGPX;
    }
    this.logger.error("FATAL!! - Root node is not gpx.");
    return ((GPX)null);
  }

  private Waypoint parseWaypoint(Node paramNode)
  {
    if (paramNode == null)
    {
      this.logger.error("null node received");
      return null;
    }
    Waypoint localWaypoint = new Waypoint();
    NamedNodeMap localNamedNodeMap = paramNode.getAttributes();
    Node localNode1 = localNamedNodeMap.getNamedItem("lat");
    if (localNode1 != null)
    {
    	
//      localObject1 = null;
      Object localObject1 = null;
      try
      {
        localObject1 = Double.valueOf(Double.parseDouble(localNode1.getNodeValue()));
      }
      catch (NumberFormatException localNumberFormatException1)
      {
        this.logger.error("bad lat value in waypoint data: " + localNode1.getNodeValue());
      }
      localWaypoint.setLatitude((Double)localObject1);
    }
    else
    {
      this.logger.warn("no lat value in waypoint data.");
    }
    Object localObject1 = localNamedNodeMap.getNamedItem("lon");
    if (localObject1 != null)
    {
//      localObject2 = null;
      Object localObject2 = null;
      try
      {
        localObject2 = Double.valueOf(Double.parseDouble(((Node)localObject1).getNodeValue()));
      }
      catch (NumberFormatException localNumberFormatException2)
      {
        this.logger.error("bad lon value in waypoint data: " + ((Node)localObject1).getNodeValue());
      }
      localWaypoint.setLongitude((Double)localObject2);
    }
    else
    {
      this.logger.warn("no lon value in waypoint data.");
    }
    Object localObject2 = paramNode.getChildNodes();
    if (localObject2 != null)
      for (int i = 0; i < ((NodeList)localObject2).getLength(); ++i)
      {
        Node localNode2 = ((NodeList)localObject2).item(i);
        if ("ele".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found ele node in waypoint data");
          localWaypoint.setElevation(getNodeValueAsDouble(localNode2));
        }
        else if ("time".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found time node in waypoint data");
          localWaypoint.setTime(getNodeValueAsDate(localNode2));
        }
        else if ("name".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found name node in waypoint data");
          localWaypoint.setName(getNodeValueAsString(localNode2));
        }
        else if ("cmt".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found cmt node in waypoint data");
          localWaypoint.setComment(getNodeValueAsString(localNode2));
        }
        else if ("desc".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found desc node in waypoint data");
          localWaypoint.setDescription(getNodeValueAsString(localNode2));
        }
        else if ("src".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found src node in waypoint data");
          localWaypoint.setSrc(getNodeValueAsString(localNode2));
        }
        else if ("magvar".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found magvar node in waypoint data");
          localWaypoint.setMagneticDeclination(getNodeValueAsDouble(localNode2));
        }
        else if ("geoidheight".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found geoidheight node in waypoint data");
          localWaypoint.setGeoidHeight(getNodeValueAsDouble(localNode2));
        }
        else if ("link".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found link node in waypoint data");
        }
        else if ("sym".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found sym node in waypoint data");
          localWaypoint.setSym(getNodeValueAsString(localNode2));
        }
        else if ("fix".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found fix node in waypoint data");
          localWaypoint.setFix(getNodeValueAsFixType(localNode2));
        }
        else if ("type".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found type node in waypoint data");
          localWaypoint.setType(getNodeValueAsString(localNode2));
        }
        else if ("sat".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found sat node in waypoint data");
          localWaypoint.setSat(getNodeValueAsInteger(localNode2));
        }
        else if ("hdop".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found hdop node in waypoint data");
          localWaypoint.setHdop(getNodeValueAsDouble(localNode2));
        }
        else if ("vdop".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found vdop node in waypoint data");
          localWaypoint.setVdop(getNodeValueAsDouble(localNode2));
        }
        else if ("pdop".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found pdop node in waypoint data");
          localWaypoint.setPdop(getNodeValueAsDouble(localNode2));
        }
        else if ("ageofdgpsdata".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found ageofgpsdata node in waypoint data");
          localWaypoint.setAgeOfGPSData(getNodeValueAsDouble(localNode2));
        }
        else if ("dgpsid".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found dgpsid node in waypoint data");
          localWaypoint.setDgpsid(getNodeValueAsInteger(localNode2));
        }
        else if ("extensions".equals(localNode2.getNodeName()))
        {
          this.logger.debug("found extensions node in waypoint data");
          Iterator localIterator = this.extensionParsers.iterator();
          while (localIterator.hasNext())
          {
            IExtensionParser localIExtensionParser = (IExtensionParser)localIterator.next();
            Object localObject3 = localIExtensionParser.parseWaypointExtension(localNode2);
            localWaypoint.addExtensionData(localIExtensionParser.getId(), localObject3);
          }
        }
      }
    else
      this.logger.debug("no child nodes found in waypoint");
    return ((Waypoint)(Waypoint)localWaypoint);
  }

  private Track parseTrack(Node paramNode)
  {
    if (paramNode == null)
    {
      this.logger.error("null node received");
      return null;
    }
    Track localTrack = new Track();
    NodeList localNodeList = paramNode.getChildNodes();
    if (localNodeList != null)
      for (int i = 0; i < localNodeList.getLength(); ++i)
      {
        Node localNode = localNodeList.item(i);
        if ("name".equals(localNode.getNodeName()))
        {
          this.logger.debug("node name found");
          localTrack.setName(getNodeValueAsString(localNode));
        }
        else if ("cmt".equals(localNode.getNodeName()))
        {
          this.logger.debug("node cmt found");
          localTrack.setComment(getNodeValueAsString(localNode));
        }
        else if ("desc".equals(localNode.getNodeName()))
        {
          this.logger.debug("node desc found");
          localTrack.setDescription(getNodeValueAsString(localNode));
        }
        else if ("src".equals(localNode.getNodeName()))
        {
          this.logger.debug("node src found");
          localTrack.setSrc(getNodeValueAsString(localNode));
        }
        else if ("link".equals(localNode.getNodeName()))
        {
          this.logger.debug("node link found");
        }
        else if ("number".equals(localNode.getNodeName()))
        {
          this.logger.debug("node number found");
          localTrack.setNumber(getNodeValueAsInteger(localNode));
        }
        else if ("type".equals(localNode.getNodeName()))
        {
          this.logger.debug("node type found");
          localTrack.setType(getNodeValueAsString(localNode));
        }
        else if ("trkseg".equals(localNode.getNodeName()))
        {
          this.logger.debug("node trkseg found");
          localTrack.setTrackPoints(parseTrackSeg(localNode));
        }
        else if ("extensions".equals(localNode.getNodeName()))
        {
          Iterator localIterator = this.extensionParsers.iterator();
          if (localIterator.hasNext())
          {
            this.logger.debug("node extensions found");
            while (true)
            {
              if (!(localIterator.hasNext()));
              IExtensionParser localIExtensionParser = (IExtensionParser)localIterator.next();
              Object localObject = localIExtensionParser.parseTrackExtension(localNode);
              localTrack.addExtensionData(localIExtensionParser.getId(), localObject);
            }
          }
        }
      }
    return localTrack;
  }

  private Route parseRoute(Node paramNode)
  {
    if (paramNode == null)
    {
      this.logger.error("null node received");
      return null;
    }
    Route localRoute = new Route();
    NodeList localNodeList = paramNode.getChildNodes();
    if (localNodeList != null)
      for (int i = 0; i < localNodeList.getLength(); ++i)
      {
        Node localNode = localNodeList.item(i);
        if ("name".equals(localNode.getNodeName()))
        {
          this.logger.debug("node name found");
          localRoute.setName(getNodeValueAsString(localNode));
        }
        else if ("cmt".equals(localNode.getNodeName()))
        {
          this.logger.debug("node cmt found");
          localRoute.setComment(getNodeValueAsString(localNode));
        }
        else if ("desc".equals(localNode.getNodeName()))
        {
          this.logger.debug("node desc found");
          localRoute.setDescription(getNodeValueAsString(localNode));
        }
        else if ("src".equals(localNode.getNodeName()))
        {
          this.logger.debug("node src found");
          localRoute.setSrc(getNodeValueAsString(localNode));
        }
        else if ("link".equals(localNode.getNodeName()))
        {
          this.logger.debug("node link found");
        }
        else if ("number".equals(localNode.getNodeName()))
        {
          this.logger.debug("node number found");
          localRoute.setNumber(getNodeValueAsInteger(localNode));
        }
        else if ("type".equals(localNode.getNodeName()))
        {
          this.logger.debug("node type found");
          localRoute.setType(getNodeValueAsString(localNode));
        }
        else
        {
          Object localObject1;
          if ("rtept".equals(localNode.getNodeName()))
          {
            this.logger.debug("node rtept found");
            localObject1 = parseWaypoint(localNode);
            if (localObject1 != null)
              localRoute.addRoutePoint((Waypoint)localObject1);
          }
          else if ("extensions".equals(localNode.getNodeName()))
          {
            localObject1 = this.extensionParsers.iterator();
            if (((Iterator)localObject1).hasNext())
            {
              this.logger.debug("node extensions found");
              while (true)
              {
                if (!(((Iterator)localObject1).hasNext()));
                IExtensionParser localIExtensionParser = (IExtensionParser)((Iterator)localObject1).next();
                Object localObject2 = localIExtensionParser.parseRouteExtension(localNode);
                localRoute.addExtensionData(localIExtensionParser.getId(), localObject2);
              }
            }
          }
        }
      }
    return ((Route)localRoute);
  }

  private ArrayList<Waypoint> parseTrackSeg(Node paramNode)
  {
    if (paramNode == null)
    {
      this.logger.error("null node received");
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    NodeList localNodeList = paramNode.getChildNodes();
    if (localNodeList != null)
      for (int i = 0; i < localNodeList.getLength(); ++i)
      {
        Node localNode = localNodeList.item(i);
        if ("trkpt".equals(localNode.getNodeName()))
        {
          this.logger.debug("node name found");
          Waypoint localWaypoint = parseWaypoint(localNode);
          if (localWaypoint != null)
            localArrayList.add(localWaypoint);
        }
        else if ("extensions".equals(localNode.getNodeName()))
        {
          this.logger.debug("node extensions found");
        }
      }
    return localArrayList;
  }

  private Double getNodeValueAsDouble(Node paramNode)
  {
    Double localDouble = null;
    try
    {
      localDouble = Double.valueOf(Double.parseDouble(paramNode.getFirstChild().getNodeValue()));
    }
    catch (Exception localException)
    {
      this.logger.error("error parsing Double value form node. val=" + paramNode.getNodeValue(), localException);
    }
    return localDouble;
  }

  private Date getNodeValueAsDate(Node paramNode)
  {
    Date localDate = null;
    try
    {
      SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss");
      localDate = localSimpleDateFormat.parse(paramNode.getFirstChild().getNodeValue());
    }
    catch (Exception localException)
    {
      this.logger.error("error parsing Date value form node. val=" + paramNode.getNodeName(), localException);
    }
    return localDate;
  }

  private String getNodeValueAsString(Node paramNode)
  {
    String str = null;
    try
    {
      str = paramNode.getFirstChild().getNodeValue();
    }
    catch (Exception localException)
    {
      this.logger.error("error getting String value form node. val=" + paramNode.getNodeName(), localException);
    }
    return str;
  }

  private FixType getNodeValueAsFixType(Node paramNode)
  {
    FixType localFixType = null;
    try
    {
      localFixType = FixType.returnType(paramNode.getFirstChild().getNodeValue());
    }
    catch (Exception localException)
    {
      this.logger.error("error getting FixType value form node. val=" + paramNode.getNodeName(), localException);
    }
    return localFixType;
  }

  private Integer getNodeValueAsInteger(Node paramNode)
  {
    Integer localInteger = null;
    try
    {
      localInteger = Integer.valueOf(Integer.parseInt(paramNode.getFirstChild().getNodeValue()));
    }
    catch (Exception localException)
    {
      this.logger.error("error parsing Integer value form node. val=" + paramNode.getNodeValue(), localException);
    }
    return localInteger;
  }

  public void writeGPX(GPX paramGPX, OutputStream paramOutputStream)
    throws ParserConfigurationException, TransformerException
  {
    DocumentBuilder localDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document localDocument = localDocumentBuilder.newDocument();
    Element localElement = localDocument.createElement("gpx");
    
    addBasicGPXInfoToNode(paramGPX, localElement, localDocument);
    if (paramGPX.getWaypoints() != null)
    {
    	Object localObject = null;
      localObject = paramGPX.getWaypoints().iterator();
      while (((Iterator)localObject).hasNext())
        addWaypointToGPXNode((Waypoint)((Iterator)localObject).next(), localElement, localDocument);
    }
    if (paramGPX.getTracks() != null)
    {
    	Object localObject = null;
      localObject = paramGPX.getTracks().iterator();
      while (((Iterator)localObject).hasNext())
        addTrackToGPXNode((Track)((Iterator)localObject).next(), localElement, localDocument);
    }
    if (paramGPX.getRoutes() != null)
    {
    	Object localObject = null;
      localObject = paramGPX.getRoutes().iterator();
      while (((Iterator)localObject).hasNext())
        addRouteToGPXNode((Route)((Iterator)localObject).next(), localElement, localDocument);
    }
    localDocument.appendChild(localElement);
    Object localObject = TransformerFactory.newInstance();
    Transformer localTransformer = ((TransformerFactory)localObject).newTransformer();
    DOMSource localDOMSource = new DOMSource(localDocument);
    StreamResult localStreamResult = new StreamResult(paramOutputStream);
    localTransformer.transform(localDOMSource, localStreamResult);
  }

  private void addWaypointToGPXNode(Waypoint paramWaypoint, Node paramNode, Document paramDocument)
  {
    addGenericWaypointToGPXNode("wpt", paramWaypoint, paramNode, paramDocument);
  }

  private void addGenericWaypointToGPXNode(String paramString, Waypoint paramWaypoint, Node paramNode, Document paramDocument)
  {
    Object localObject1;
    Object localObject2;
    Element localElement = paramDocument.createElement(paramString);
    NamedNodeMap localNamedNodeMap = localElement.getAttributes();
    if (paramWaypoint.getLatitude() != null)
    {
      localObject1 = paramDocument.createAttribute("lat");
      ((Node)localObject1).setNodeValue(paramWaypoint.getLatitude().toString());
      localNamedNodeMap.setNamedItem((Node)localObject1);
    }
    if (paramWaypoint.getLongitude() != null)
    {
      localObject1 = paramDocument.createAttribute("lon");
      ((Node)localObject1).setNodeValue(paramWaypoint.getLongitude().toString());
      localNamedNodeMap.setNamedItem((Node)localObject1);
    }
    if (paramWaypoint.getElevation() != null)
    {
      localObject1 = paramDocument.createElement("ele");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getElevation().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getTime() != null)
    {
      localObject1 = paramDocument.createElement("time");
      localObject2 = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ss'Z'");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(((SimpleDateFormat)localObject2).format(paramWaypoint.getTime())));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getMagneticDeclination() != null)
    {
      localObject1 = paramDocument.createElement("magvar");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getMagneticDeclination().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getGeoidHeight() != null)
    {
      localObject1 = paramDocument.createElement("geoidheight");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getGeoidHeight().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getName() != null)
    {
      localObject1 = paramDocument.createElement("name");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getName()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getComment() != null)
    {
      localObject1 = paramDocument.createElement("cmt");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getComment()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getDescription() != null)
    {
      localObject1 = paramDocument.createElement("desc");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getDescription()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getSrc() != null)
    {
      localObject1 = paramDocument.createElement("src");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getSrc()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getSym() != null)
    {
      localObject1 = paramDocument.createElement("sym");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getSym()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getType() != null)
    {
      localObject1 = paramDocument.createElement("type");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getType()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getFix() != null)
    {
      localObject1 = paramDocument.createElement("fix");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getFix().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getSat() != null)
    {
      localObject1 = paramDocument.createElement("sat");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getSat().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getHdop() != null)
    {
      localObject1 = paramDocument.createElement("hdop");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getHdop().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getVdop() != null)
    {
      localObject1 = paramDocument.createElement("vdop");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getVdop().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getPdop() != null)
    {
      localObject1 = paramDocument.createElement("pdop");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getPdop().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getAgeOfGPSData() != null)
    {
      localObject1 = paramDocument.createElement("ageofdgpsdata");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getAgeOfGPSData().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getDgpsid() != null)
    {
      localObject1 = paramDocument.createElement("dgpsid");
      ((Node)localObject1).appendChild(paramDocument.createTextNode(paramWaypoint.getDgpsid().toString()));
      localElement.appendChild((Node)localObject1);
    }
    if (paramWaypoint.getExtensionsParsed() > 0)
    {
      localObject1 = paramDocument.createElement("extensions");
      localObject2 = this.extensionParsers.iterator();
      while (((Iterator)localObject2).hasNext())
        ((IExtensionParser)((Iterator)localObject2).next()).writeWaypointExtensionData((Node)localObject1, paramWaypoint, paramDocument);
      localElement.appendChild((Node)localObject1);
    }
    paramNode.appendChild(localElement);
  }

  private void addTrackToGPXNode(Track paramTrack, Node paramNode, Document paramDocument)
  {
    Element localElement2;
    Iterator localIterator;
    Element localElement1 = paramDocument.createElement("trk");
    if (paramTrack.getName() != null)
    {
      localElement2 = paramDocument.createElement("name");
      localElement2.appendChild(paramDocument.createTextNode(paramTrack.getName()));
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getComment() != null)
    {
      localElement2 = paramDocument.createElement("cmt");
      localElement2.appendChild(paramDocument.createTextNode(paramTrack.getComment()));
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getDescription() != null)
    {
      localElement2 = paramDocument.createElement("desc");
      localElement2.appendChild(paramDocument.createTextNode(paramTrack.getDescription()));
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getSrc() != null)
    {
      localElement2 = paramDocument.createElement("src");
      localElement2.appendChild(paramDocument.createTextNode(paramTrack.getSrc()));
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getNumber() != null)
    {
      localElement2 = paramDocument.createElement("number");
      localElement2.appendChild(paramDocument.createTextNode(paramTrack.getNumber().toString()));
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getType() != null)
    {
      localElement2 = paramDocument.createElement("type");
      localElement2.appendChild(paramDocument.createTextNode(paramTrack.getType()));
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getExtensionsParsed() > 0)
    {
      localElement2 = paramDocument.createElement("extensions");
      localIterator = this.extensionParsers.iterator();
      while (localIterator.hasNext())
        ((IExtensionParser)localIterator.next()).writeTrackExtensionData(localElement2, paramTrack, paramDocument);
      localElement1.appendChild(localElement2);
    }
    if (paramTrack.getTrackPoints() != null)
    {
      localElement2 = paramDocument.createElement("trkseg");
      localIterator = paramTrack.getTrackPoints().iterator();
      while (localIterator.hasNext())
        addGenericWaypointToGPXNode("trkpt", (Waypoint)localIterator.next(), localElement2, paramDocument);
      localElement1.appendChild(localElement2);
    }
    paramNode.appendChild(localElement1);
  }

  private void addRouteToGPXNode(Route paramRoute, Node paramNode, Document paramDocument)
  {
    Object localObject;
    Element localElement = paramDocument.createElement("rte");
    if (paramRoute.getName() != null)
    {
      localObject = paramDocument.createElement("name");
      ((Node)localObject).appendChild(paramDocument.createTextNode(paramRoute.getName()));
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getComment() != null)
    {
      localObject = paramDocument.createElement("cmt");
      ((Node)localObject).appendChild(paramDocument.createTextNode(paramRoute.getComment()));
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getDescription() != null)
    {
      localObject = paramDocument.createElement("desc");
      ((Node)localObject).appendChild(paramDocument.createTextNode(paramRoute.getDescription()));
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getSrc() != null)
    {
      localObject = paramDocument.createElement("src");
      ((Node)localObject).appendChild(paramDocument.createTextNode(paramRoute.getSrc()));
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getNumber() != null)
    {
      localObject = paramDocument.createElement("number");
      ((Node)localObject).appendChild(paramDocument.createTextNode(paramRoute.getNumber().toString()));
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getType() != null)
    {
      localObject = paramDocument.createElement("type");
      ((Node)localObject).appendChild(paramDocument.createTextNode(paramRoute.getType()));
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getExtensionsParsed() > 0)
    {
      localObject = paramDocument.createElement("extensions");
      Iterator localIterator = this.extensionParsers.iterator();
      while (localIterator.hasNext())
        ((IExtensionParser)localIterator.next()).writeRouteExtensionData((Node)localObject, paramRoute, paramDocument);
      localElement.appendChild((Node)localObject);
    }
    if (paramRoute.getRoutePoints() != null)
    {
      localObject = paramRoute.getRoutePoints().iterator();
      while (((Iterator)localObject).hasNext())
        addGenericWaypointToGPXNode("rtept", (Waypoint)((Iterator)localObject).next(), localElement, paramDocument);
    }
    paramNode.appendChild(localElement);
  }

  private void addBasicGPXInfoToNode(GPX paramGPX, Node paramNode, Document paramDocument)
  {
    Object localObject;
    NamedNodeMap localNamedNodeMap = paramNode.getAttributes();
    if (paramGPX.getVersion() != null)
    {
      localObject = paramDocument.createAttribute("version");
      ((Node)localObject).setNodeValue(paramGPX.getVersion());
      localNamedNodeMap.setNamedItem((Node)localObject);
    }
    if (paramGPX.getCreator() != null)
    {
      localObject = paramDocument.createAttribute("creator");
      ((Node)localObject).setNodeValue(paramGPX.getCreator());
      localNamedNodeMap.setNamedItem((Node)localObject);
    }
    if (paramGPX.getExtensionsParsed() > 0)
    {
      localObject = paramDocument.createElement("extensions");
      Iterator localIterator = this.extensionParsers.iterator();
      while (localIterator.hasNext())
        ((IExtensionParser)localIterator.next()).writeGPXExtensionData((Node)localObject, paramGPX, paramDocument);
      paramNode.appendChild((Node)localObject);
    }
  }
}