package com.zondy.mapgis.android.model;

public class EntityUser {
	String tid;
	String nickname;
	String userName;
	String password;
	String appAlias;
	boolean isChecked;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public EntityUser() {
	}

	public EntityUser(String tid,String userName,String nickname, String password, boolean isChecked) {
		super();
		this.tid = tid;
		this.userName = userName;
		this.password = password;
		this.isChecked = isChecked;
		this.nickname = nickname;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getAppAlias() {
		return appAlias;
	}

	public void setAppAlias(String appAlias) {
		this.appAlias = appAlias;
	}
	
	
}
