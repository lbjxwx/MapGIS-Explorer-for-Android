package com.zondy.mapgis.android.frag;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.utils.DialogUtil;
import com.zondy.mapgis.android.utils.SharedUtils;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.map.model.CommAdapter;
import com.zondy.mapgis.map.model.CommField;
import com.zondy.mapgis.map.model.MapInfo;
import com.zondy.mapgis.explorer.R;

public class MapxChooseFrag extends BaseFragment
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;
	private String MAPROOT_PATH = MapApplication.SystemLibraryPath + "Map";
	
	//当前文件夹下所有的地图文档
	List<File> mMapFileLst = new ArrayList<File>();
	private ListView lvFiles;
	private CommAdapter mMapFileAdapter = new CommAdapter();

	@Override
	public int bindLayout()
	{
		return R.layout.mapx_manager_frag;
	}

	@Override
	public void initView(View view)
	{
		lvFiles = (ListView) view.findViewById(R.id.files);
	}

	@Override
	public void doBusiness(Context mContext)
	{
		mMapActivity = MapApplication.getApp().getMapActivity();
		

		File rootFile = new File(MAPROOT_PATH);
		//判断地图路径是否存在
		if(rootFile.exists())
		{
			mMapFileLst = getMapFiles(getAllFile(rootFile));
//			inflateListView(mMapFileLst);
			inflateMapListView(mMapFileLst);
		}
		
		//ListView点击事件
		lvFiles.setOnItemClickListener(new OnItemClickListener()
		{

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id)
			{
				View loadView = getActivity().getLayoutInflater().inflate(R.layout.view_loadmapx_alertdialog, null);
				Button okBtn = (Button) loadView.findViewById(R.id.ok_btn);
				Button cancleBtn = (Button) loadView.findViewById(R.id.cancle_btn);
				AlertDialog.Builder alBuilder = DialogUtil.createNobuttonDialog(getContext(), null, null, loadView, null);
				final Dialog dialog = alBuilder.show();
				
				okBtn.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View arg0)
					{
						String mapDataPath = mMapFileLst.get(position).getAbsolutePath();
						// 设置地图数据路径
						MapInfo mapInfo = new MapInfo();
						mapInfo.setId(position);
						mapInfo.setMapType(MapActivity.MAPX);
						mapInfo.setMapxPath(mapDataPath);
						SharedUtils.getInstance().saveMapInfo(getContext(), mapInfo);
						mMapActivity.initMap();
						mMapFileAdapter.notifyDataSetChanged();
						dialog.dismiss();
					}
				});
				cancleBtn.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View arg0)
					{
						dialog.dismiss();
					}
				});
//				DialogUtil.createTwoButtonDialog(getContext(), null, "加载该地图文档", null, null, "确定", "取消",
//						new DialogInterface.OnClickListener()
//						{
//							@Override
//							public void onClick(DialogInterface arg0, int arg1)
//							{
//								String mapDataPath = mMapFileLst.get(position).getAbsolutePath();
//								// 设置地图数据路径
//								MapInfo mapInfo = new MapInfo();
//								mapInfo.setId(position);
//								mapInfo.setMapType(MapActivity.MAPX);
//								mapInfo.setMapxPath(mapDataPath);
//								SharedUtils.getInstance().saveMapInfo(getContext(), mapInfo);
//								mMapActivity.initMap();
//								mMapFileAdapter.notifyDataSetChanged();
//							}
//						}, null).show();
				
			}
		});
	
		
	}

	/**
	 * 获取当前文件夹下所有文件
	 * @param rootFile
	 * @return
	 */
	private List<File> getAllFile(File rootFile)
	{
		//当前路径下所有文件
		List<File> allFiles = new ArrayList<File>();
		
		File[] mRootFiles  = rootFile.listFiles();
		//根目录
		for(int i = 0;i < mRootFiles.length;i++)
		{
			if(mRootFiles[i].isFile())
			{
				allFiles.add(mRootFiles[i]);
			}
			else
			{
				//二级目录
				File[] secondFiles = mRootFiles[i].listFiles();
				for(int j = 0; j < secondFiles.length;j++)
				{
					if(secondFiles[j].isFile())
					{
						allFiles.add(secondFiles[j]);
					}
					else
					{
						//三级目录
						File[] thirdFiles = secondFiles[j].listFiles();
						for(int x = 0;x < thirdFiles.length;x++)
						{
							if(thirdFiles[x].isFile())
							{
								allFiles.add(thirdFiles[x]);
							}
						}
					}
				}
			}
		}
		return allFiles;
	}
	/**
	 * 获取所有的地图文档
	 * @param filelst
	 * @return
	 */
	private List<File> getMapFiles(List<File> filelst)
	{
		List<File> mapfile = new ArrayList<File>();
		for(int i = 0; i < filelst.size(); i++)
		{
			String fileName = filelst.get(i).getName();
			String prefix = fileName.substring(fileName.lastIndexOf(".")+1);
			
			if(prefix.equalsIgnoreCase("mapx") || prefix.equalsIgnoreCase("xml"))
			{
				mapfile.add(filelst.get(i));
//				Log.d(TAG, "fileName:"+fileName);
			}
		}
		return mapfile;
		
	}
	
	/**
	 * 填充LisView
	 * @param mapFile
	 */
	private void inflateMapListView(List<File> mapFileLst)
	{
		List<CommField> commFieldLst = new ArrayList<CommField>();
		for(int i = 0; i < mapFileLst.size(); i++)
		{
			CommField commField = new CommField();
			commField.setStrName(mapFileLst.get(i).getName());
			commFieldLst.add(commField);
		}
		
		mMapFileAdapter.setContext(getContext());
		mMapFileAdapter.setListField(commFieldLst);
		
		lvFiles.setAdapter(mMapFileAdapter);
	}
	
	
	/**
	 * 根据文件夹填充ListView
	 * 
	 * @param files
	 */
	private void inflateListView(List<File> mapFile)
	{

		List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();

		for (int i = 0; i < mapFile.size(); i++)
		{

			Map<String, Object> listItem = new HashMap<String, Object>();

			if (mapFile.get(i).isDirectory())
			{
				// 如果是文件夹就显示的图片为文件夹的图片
				listItem.put("icon", R.drawable.folder);
			}
			else
			{
				listItem.put("icon", R.drawable.file);
			}
			// 添加一个文件名称
			listItem.put("filename", mapFile.get(i).getName());


			// 获取文件的最后修改日期
//			long modTime = mapFile.get(i).lastModified();
//			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			System.out.println(dateFormat.format(new Date(modTime)));

			// 添加一个最后修改日期
//			listItem.put("modify", "修改日期:" + dateFormat.format(new Date(modTime)));

			listItems.add(listItem);
		}

		// 定义一个SimpleAdapter
		SimpleAdapter adapter = new SimpleAdapter(getContext(), listItems, R.layout.filelist_item, new String[] { "filename", "icon",
				"modify" }, new int[] { R.id.file_name, R.id.icon, R.id.file_modify });

		// 填充数据集
		lvFiles.setAdapter(adapter);

	}

	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
