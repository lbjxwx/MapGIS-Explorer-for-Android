package com.zondy.mapgis.android.frag;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.TextView;

import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.base.BaseFragment;
import com.zondy.mapgis.android.service.LocationService;
import com.zondy.mapgis.android.view.CustomNestRadioGroup;
import com.zondy.mapgis.android.view.CustomNestRadioGroup.OnCheckedChangeListener;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.explorer.R;

/**
 * 设置
 * 
 * @author fjl
 * 
 */
public class TracksRecordSettingFrag extends BaseFragment implements OnClickListener
{
	private String TAG = this.getClass().getSimpleName();
	private MapActivity mMapActivity = null;

	private TextView mTitleTxt = null;
	private RadioButton mTimeRb = null;
	private RadioButton mDisRb = null;
	public LocationMode tempMode = LocationMode.Device_Sensors;
	private LocationClientOption mOption;
	private LocationService locService;
	

	@Override
	public int bindLayout()
	{
		return R.layout.record_setting_frag;
	}

	@Override
	public void initView(View view)
	{
		mTitleTxt = (TextView) view.findViewById(R.id.title_text);
		mTitleTxt.setText("轨迹记录参数");
		mTimeRb = (RadioButton) view.findViewById(R.id.one_second_rb);
		mDisRb = (RadioButton) view.findViewById(R.id.record_meter_rb5);
		
		view.findViewById(R.id.title_back).setOnClickListener(this);
		
		// 修改RadioGroup源码后的自定义控件,可以查找RadioGroup里面不同布局RadioButton
		CustomNestRadioGroup timeRadioGroup = (CustomNestRadioGroup) view.findViewById(R.id.trackrecord_time_rg);
		if (timeRadioGroup != null)
		{
			timeRadioGroup.setOnCheckedChangeListener(new TimeRadioListen());
		}
		CustomNestRadioGroup disRadioButton = (CustomNestRadioGroup) view.findViewById(R.id.trackrecord_dis_rg);
		if(disRadioButton != null)
		{
			disRadioButton.setOnCheckedChangeListener(new DisRadioListen());
		}
		
		CustomNestRadioGroup coorTypeRG = (CustomNestRadioGroup) view.findViewById(R.id.coortype_rg);
		if(coorTypeRG != null)
		{
			//初始化监听
			coorTypeRG.setOnCheckedChangeListener(new setCoorTypeListent());
		}
		
	}

	@Override
	public void doBusiness(Context mContext)
	{
		mMapActivity = MapApplication.getApp().getMapActivity();
		
		locService =  MapApplication.getApp().locationService;
		mOption = locService.getDefaultLocationClientOption();
		locService.stop();
		mTimeRb.setChecked(true);
		mDisRb.setChecked(true);
	}

	class TimeRadioListen implements OnCheckedChangeListener
	{

		@Override
		public void onCheckedChanged(CustomNestRadioGroup group, int checkedId)
		{
			int recordTime = 1000;
			
			if (group.getId() == R.id.trackrecord_time_rg)
			{
				if (checkedId == R.id.one_second_rb)
				{
					recordTime = 1000;
				}
				
				else if (checkedId == R.id.three_second_rb)
				{
					recordTime = 3000;
				}
				else if (checkedId == R.id.five_second_rb)
				{
					recordTime = 5000;
				}
				else if (checkedId == R.id.teen_second_rb)
				{
					recordTime = 10000;
				}
				else if (checkedId == R.id.time_rb30)
				{
					recordTime = 30000;
				}
				else if (checkedId == R.id.time_rb30)
				{
					recordTime = 60000;
				}
				mOption.setTimeOut(recordTime);
			}
		}

	}
	
	class DisRadioListen implements OnCheckedChangeListener
	{

		@Override
		public void onCheckedChanged(CustomNestRadioGroup group, int checkedId)
		{
			if (group.getId() == R.id.trackrecord_dis_rg)
			{
				if (checkedId == R.id.record_meter_rb5)
				{
					tempMode = LocationMode.Device_Sensors;
				}
				else if (checkedId == R.id.record_meter_rb10)
				{
					tempMode = LocationMode.Hight_Accuracy;
				}
				else
				{
					tempMode = LocationMode.Battery_Saving;
				}
				
				mOption.setLocationMode(tempMode);
			}
		}
		
	}
	class setCoorTypeListent implements OnCheckedChangeListener
	{

		@Override
		public void onCheckedChanged(CustomNestRadioGroup group, int checkedId)
		{
			if(group.getId() == R.id.coortype_rg)
			{
				if(checkedId == R.id.coortype_rb5)
				{
					mOption.setCoorType("gcj02");
				}
				if(checkedId == R.id.coortype_rb10)
				{
					mOption.setCoorType("bd09ll");
				}
				if(checkedId == R.id.coortype_rb20)
				{
					mOption.setCoorType("bd09");
				}
				
			}
		}
		
	}

	@Override
	public void onDestroyView()
	{
		onBack();
		super.onDestroyView();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.title_back:
			mMapActivity.removeFragment(TAG, null);
			break;

		default:
			break;
		}

	}

	/**
	 * 销毁视图事件处理
	 */
	private void onBack()
	{
		/**
		 * 设置前需停止定位服务，设置后重启定位服务才可以生效
		 */
		locService.setLocationOption(mOption);
		MapApplication.getApp().setIsRecodSetting(true);
		mMapActivity.mMapRightContent.setVisibility(View.GONE);
	}

	@Override
	public void onResume()
	{
		getFocus();
		super.onResume();
	}
	/**
	 * 捕捉返回事件
	 * 说明：本方式适用于Fragment页面中没有其他可以获取焦点的View（如EditText）
	 */
	private void getFocus() {
	    getView().setFocusable(true);
	    getView().setFocusableInTouchMode(true);
	    getView().requestFocus();
	    getView().setOnKeyListener(new View.OnKeyListener() {
	 
	        @Override
	        public boolean onKey(View v, int keyCode, KeyEvent event) {
	            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK) {
	                // 监听到返回按钮点击事件
	            	mMapActivity.removeFragment(TAG, null);
	                return false;
	            }
//	            return false;//未处理
	            return true;
	        }
	    });
	}
	@Override
	public boolean goback()
	{
		return false;
	}

	@Override
	public boolean isLocked()
	{
		return false;
	}

	@Override
	public String getName()
	{
		return TAG;
	}

}
