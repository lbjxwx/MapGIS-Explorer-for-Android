package com.zondy.mapgis.android.utils;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Log;

import com.zondy.mapgis.android.annotation.Annotation;
import com.zondy.mapgis.android.annotation.AnnotationView;
import com.zondy.mapgis.android.annotation.AnnotationsOverlay;
import com.zondy.mapgis.android.graphic.GraphicImage;
import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.android.mapview.MapView.MapViewAnnotationListener;
import com.zondy.mapgis.core.geometry.Dot;
import com.zondy.mapgis.core.geometry.Rect;
import com.zondy.mapgis.explorer.MapApplication;
import com.zondy.mapgis.map.model.SearchAnnotation;
import com.zondy.mapgis.explorer.R;

public class SearchUtils
{
	private static String TAG = SearchUtils.class.getSimpleName(); 
	public static SearchUtils searchUtils = null;
	public Context mContext = null;
	
	private final static int[] mSearchIco = { R.drawable.ico_result_location_1, R.drawable.ico_result_location_2, R.drawable.ico_result_location_3, R.drawable.ico_result_location_4,
			R.drawable.ico_result_location_5, R.drawable.ico_result_location_6, R.drawable.ico_result_location_7, R.drawable.ico_result_location_8, R.drawable.ico_result_location_9,
			R.drawable.ico_result_location_10, R.drawable.ico_result_location_11, R.drawable.ico_result_location_22, R.drawable.ico_result_location_33, R.drawable.ico_result_location_44,
			R.drawable.ico_result_location_55, R.drawable.ico_result_location_66, R.drawable.ico_result_location_77, R.drawable.ico_result_location_88, R.drawable.ico_result_location_99,
			R.drawable.ico_result_location_100 };
	
	
	public static SearchUtils getInstance()
	{
		if(searchUtils == null)
		{
			searchUtils = new SearchUtils();
		}
		
		return searchUtils;
	}
	

	/**
	 * 添加单个annotation
	 * 
	 * @param context
	 * @param title
	 * @param jsonDescription
	 * @param latitude
	 * @param longitude
	 * @param imgResourceId 标注的图片id
	 * @return
	 */
	public static Annotation addAnnotation(Context context, MapView mapView, String uid, String title, String jsonDescription, double longitude,
			double latitude, int imgResourceId,int id)
	{
		Bitmap bmp = ((BitmapDrawable) context.getResources().getDrawable(imgResourceId)).getBitmap();
		return addAnnotation(context, mapView, uid, title, jsonDescription, longitude, latitude, bmp,id);
	}
	
	/**
	 * 添加标注
	 * 
	 * @param context
	 * @param mapView
	 * @param uid
	 * @param title
	 * @param jsonDescription
	 * @param latitude
	 * @param longitude
	 * @param bitmap
	 * @return
	 */
	public static Annotation addAnnotation(Context context, MapView mapView, String uid, String title, String jsonDescription, double longitude,
			double latitude, Bitmap bitmap,int id)
	{
//		if (latitude > 180 && longitude > 90)
//		{
			Dot dot = PointTransferUtil.Lonlat2Mercator(new Dot(longitude, latitude));
			SearchAnnotation searchAnnotation;
			// 添加地图标注
			if (!TextUtils.isEmpty(uid))
			{
//				searchAnnotation = new SearchAnnotation(uid, title, jsonDescription, dot, bitmap);
				searchAnnotation = new SearchAnnotation(title, jsonDescription, dot, bitmap, id);
			}
			else
			{
				searchAnnotation = new SearchAnnotation(title, jsonDescription, dot, bitmap,id);
			}
			mapView.getAnnotationsOverlay().addAnnotation(searchAnnotation);
			return searchAnnotation;
//		}
//		else
//		{
//			Log.e(TAG, "经纬度有误");
//			return null;
//		}
	}
	
	/**
	 * Graphics放大到当前显示范围
	 * @param context
	 * @param mapView
	 * @param rectLst
	 */
	public static void zoomToGraphicsRect(Context context, MapView mapView, List<Rect> rectLst)
	{
		if(rectLst.size() < 1)
		{
			Log.e(TAG, "rectLst is null!!!");
		}
		
		double xMin = 0.0;
		double yMin = 0.0;
		double xMax = 0.0;
		double yMax = 0.0;
		
		for(int i = 0;i < rectLst.size();i++)
		{
			double xCurMin = rectLst.get(i).xMin;
			double yCurMin = rectLst.get(i).yMin;
			double xCurMax = rectLst.get(i).xMax;
			double yCurMax = rectLst.get(i).yMax;
			
			if(i == 0)
			{
				xMin = xCurMin;
				xMax = xCurMax;
				yMin = yCurMin;
				yMax = yCurMax;
			}
			else
			{
				xMin = xMin < xCurMin ? xMin : xCurMin;
				yMin = yMin < yCurMin ? yMin : yCurMin;
				xMax = xMax > xCurMax ? xMax : xCurMax;
				yMax = yMax > yCurMax ? yMax :yCurMax;
				
			}
			
		}
		//rectLst最大外包矩形
		Rect graphicRect = new Rect(xMin, yMin, xMax, yMax);
		mapView.zoomToRange(graphicRect, true);
	}
	
	/**
	 * 在地图上添加GraphicImage
	 * @param contex
	 * @param mapview
	 * @param icoId
	 * @param curDot
	 */
	public static void drawGraphicImageOnMap(Context contex,MapView mapview,int icoId,Dot curDot)
	{
		Bitmap bmp = BitmapFactory.decodeResource(contex.getResources(), icoId);
		GraphicImage graphicImg = new GraphicImage();
		graphicImg.setImage(bmp);
		graphicImg.setPoint(curDot);
		graphicImg.setAnchorPoint(new PointF(0.5f, 0f));
		mapview.getGraphicsOverlay().addGraphic(graphicImg);
		mapview.refresh();
		
	}
	
	public static class MapSearchAnnotationListen implements MapViewAnnotationListener
	{

		@Override
		public void mapViewClickAnnotation(MapView mapView, Annotation annotation)
		{
			AnnotationsOverlay annotationLayer = mapView.getAnnotationsOverlay();
			if(annotationLayer != null)
			{
				List<Annotation> annotationLst = mapView.getAnnotationsOverlay().getAllAnnotations();
				if(annotation instanceof SearchAnnotation)
				{
					int anntationId = ((SearchAnnotation) annotation).getId();
					Bitmap redBmp = BitmapFactory.decodeResource(MapApplication.gainContext().getResources(), mSearchIco[anntationId + 10]);
					for(int i = 0;i < annotationLst.size(); i++)
					{
						Bitmap bmp = BitmapFactory.decodeResource(MapApplication.gainContext().getResources(),  mSearchIco[i]);
						annotationLst.get(i).setImage(bmp);
					}
					annotationLst.get(anntationId).setImage(redBmp);
					mapView.panToCenter(annotation.getPoint(), true);
					
//					int index = annotationLayer.indexOf(annotation);
//					annotationLayer.moveAnnotation(index, -1);
					mapView.refresh();
//					ToolToast.showShort(""+annotationLst.size());
				}
				
			}
			
		}

		@Override
		public boolean mapViewWillShowAnnotationView(MapView mapView, AnnotationView annotationView)
		{
			return false;
		}

		@Override
		public boolean mapViewWillHideAnnotationView(MapView mapView, AnnotationView annotationView)
		{
			return false;
		}

		@Override
		public AnnotationView mapViewViewForAnnotation(MapView mapView, Annotation annotation)
		{
			return null;
		}

		@Override
		public void mapViewClickAnnotationView(MapView mapView, AnnotationView annotationView)
		{
			
		}
		
	}

}
