package com.zondy.mapgis.android.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import com.zondy.mapgis.android.model.EntityUser;
import com.zondy.mapgis.core.map.MapServerType;
import com.zondy.mapgis.explorer.FilePathCfg;
import com.zondy.mapgis.map.model.MapInfo;

public class SharedUtils
{

	String preferenceName = "smart_mobile";

	private static SharedUtils instance = null;

	public static SharedUtils getInstance()
	{
		if (instance == null)
		{
			instance = new SharedUtils();
		}
		return instance;
	}

	/**
	 * 保存地图信息
	 * 
	 * @param contenx
	 * @param mapInfo
	 */
	public void saveMapInfo(Context contenx, MapInfo mapInfo)
	{
		SharedPreferences preferences = contenx.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putInt("id", mapInfo.getId());
		editor.putInt("mapType", mapInfo.getMapType());
		editor.putString("mapxPath", mapInfo.getMapxPath());
		editor.putString("igserverAddress", mapInfo.getIgserverAddress());
		editor.putString("mapxPath", mapInfo.getMapxPath());
		editor.putString("mapServerType", mapInfo.getMapServerType());
		editor.putString("serverUrl", mapInfo.getServerUrl());
		editor.putString("serverName", mapInfo.getServerName());
		editor.commit();
	}

	/**
	 * 获取地图信息
	 */
	public MapInfo getMapInfo(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		int id = preferences.getInt("id", 0);
		int mapType = preferences.getInt("mapType", 0);
		String mapxPath = preferences.getString("mapxPath", FilePathCfg.MAPX_CHINA_PATH);
		String igserverAddress = preferences.getString("igserverAddress", "");
		String mapServerType = preferences.getString("mapServerType", MapServerType.BaiduMap.name());
		String serverUrl = preferences.getString("serverUrl", "");
		String serverName = preferences.getString("serverName", "");
		MapInfo mapInfo = new MapInfo(id,mapType, mapxPath, igserverAddress, mapServerType,serverUrl,serverName);
		return mapInfo;
	}
	/**
	 * 保存用户信息
	 * 
	 * @param context
	 * @param entityUser
	 */
	public void saveUserInfo(Context context, EntityUser entityUser)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putString("tid", entityUser.getTid());
		editor.putString("userName", entityUser.getUserName());
		editor.putString("nickname", entityUser.getNickname());
		editor.putString("password", entityUser.getPassword());
		editor.commit();
	}

	/**
	 * 获取用户信息
	 * 
	 * @param context
	 * @return
	 */
	public EntityUser getUserInfo(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		String tid = preferences.getString("tid", "");
		String nickname = preferences.getString("nickname", "");
		String userName = preferences.getString("userName", "");
		String password = preferences.getString("password", "");
		EntityUser entityUser = new EntityUser(tid, userName, nickname, password, false);
		return entityUser;
	}

	/**
	 * 清除用户信息
	 * 
	 * @param context
	 * @param entityUser
	 */
	public void clearUserInfo(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.remove("userName");
		editor.remove("password");
		editor.remove("tid");
		editor.remove("nickname");
		editor.commit();
	}
	/**
	 * 用户是否登录
	 * 
	 * @param context
	 * @return
	 */
	public boolean isLogin(Context context)
	{
		EntityUser user = getUserInfo(context);
		if (TextUtils.isEmpty(user.getUserName()))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * 保存地图版本信息
	 * 
	 * @param context
	 * @param mapVersion
	 */
	public void saveMapVersion(Context context, String mapVersion)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putString("mapVersion", mapVersion);
		editor.commit();
	}

	/**
	 * 获取地图版本信息
	 * 
	 * @param context
	 * @return
	 */
	public String getMapVersion(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		String updateVersionName = preferences.getString("mapVersion", "");
		return updateVersionName;
	}

	/**
	 * 保存当前在线地图文档的版本信息
	 * 
	 * @param context
	 * @param currentMapVersion
	 */
	public boolean saveCurrentMapVersion(Context context, String currentMapVersion)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putString("currentMapVersion", currentMapVersion);
		return editor.commit();
	}

	/**
	 * 获取当前在线地图文档的版本信息
	 * 
	 * @param context
	 * @return
	 */
	public String getCurrentMapVersion(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		String updateVersionName = preferences.getString("currentMapVersion", "0");
		return updateVersionName;
	}

	/**
	 * 获取风电场信息
	 * 
	 * @param context
	 * @return
	 */
	public String getFarmInfo(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		String farmJsonStr = preferences.getString("farmJsonStr", "");
		return farmJsonStr;
	}

	public boolean clearCollectingInfo(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.remove("collect_userId");
		editor.remove("collect_state");
		editor.remove("collect_startTime");
		editor.remove("collect_pathNo");
		editor.remove("collect_pathName");
		return editor.commit();
	}

	/**
	 * 保存是否是新用户登陆，用在切换用户时清除地图上的标注信息
	 * 
	 * @param context
	 * @param isNewUser
	 * @return
	 */
	public boolean saveIsNewUser(Context context, boolean isNewUser)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putBoolean("isNewUser", isNewUser);
		return editor.commit();
	}

	/**
	 * 获取是否是新用户
	 * 
	 * @param context
	 * @return
	 */
	public boolean isNewUser(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		return preferences.getBoolean("isNewUser", false);
	}

	/**
	 * 保存是否第一次安装应用
	 * 
	 * @Method: com.zondy.jinfeng.util.SharedTool.saveIsFirstInstallApp
	 * @Description: TODO
	 * @param @param context
	 * @param @param isFirstInstall
	 * @param @return
	 * @return boolean
	 * @throws
	 */
	public boolean saveIsFirstInstallApp(Context context, boolean isFirstInstall)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		Editor editor = preferences.edit();
		editor.putBoolean("isFirstInstall", isFirstInstall);
		return editor.commit();
	}

	/**
	 * 获取是否第一次安装应用
	 * 
	 * @Method: com.zondy.jinfeng.util.SharedTool.getIsFirstInstallApp
	 * @Description: TODO
	 * @param @param context
	 * @param @return
	 * @return boolean
	 * @throws
	 */
	public boolean getIsFirstInstallApp(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(preferenceName, Activity.MODE_MULTI_PROCESS);
		return preferences.getBoolean("isFirstInstall", true);
	}
}
