package com.zondy.mapgis.android.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.zondy.mapgis.android.map.dao.IListener;
import com.zondy.mapgis.explorer.R;

public final class DialogUtil
{
	/**
	 * 进度提示框
	 */
	public static ProgressDialog progressDialog = null;
	public static ProgressDialog horizontalProgressDialog = null;

	private static Object lock = new Object();

	private static volatile ProgressDialog progressDlg;
	private static final int MSG_HIDE_PROGRESSDLG = 0x0001;
	/**
	 * 进度条显示的最长时间
	 */
	private static final int PROGRESSDLG_MAXTIME = 1000 * 600;

	// 理论上说这个Handler应该是会在主线程中初始化的

	private static Handler handlerProgressDlg = new Handler()
	{
		@Override
		public void handleMessage(android.os.Message msg)
		{
			switch (msg.what)
			{
			case MSG_HIDE_PROGRESSDLG:
				LogTool.i("DialogTool.handlerProgressDlg", "ProgressDialog显示时间超过了 " + PROGRESSDLG_MAXTIME + "， 强制关闭之");
				hidenProgressDlg();
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 显示一个{@link ProgressDialog}，同一时刻只有一个。<br>
	 * 必须在UI线程中调用
	 * 
	 * @param context
	 * @param strMessage
	 */
	public static void showProgressDlg(Context context, String strMessage)
	{
		synchronized (lock)
		{
			// 如果重复利用已存在的 ProgressDialog
			// ，则可能会存在前后两个所在Context不一致的问题，
			// 导致报错 BadWindowToken.
			// if (null == progressDlg) {
			LogTool.i("ProgressDlg", "show");
			progressDlg = new ProgressDialog(context, ProgressDialog.THEME_HOLO_LIGHT);
			progressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			// progressDlg.setTitle(context.getString(R.string.app_name));
			progressDlg.setIndeterminate(false);
			progressDlg.setCancelable(false);
			// progressDlg.setIcon(R.drawable.logo);

			// }
			progressDlg.setMessage(strMessage);
			progressDlg.show();

			// 打开进度条后，超时时间过后，发送消息强制关闭进度条，以避免某些情况下可能忘了关闭进度条。
			handlerProgressDlg.postDelayed(new Runnable()
			{

				@Override
				public void run()
				{
					if (progressDlg != null && progressDlg.isShowing())
					{
						Message.obtain(handlerProgressDlg, MSG_HIDE_PROGRESSDLG).sendToTarget();
					}
				}
			}, PROGRESSDLG_MAXTIME);

		}
	}

	/**
	 * 如果{@link ProgressDialog}正在显示，隐藏{@link ProgressDialog}。
	 */
	public static void hidenProgressDlg()
	{

		synchronized (lock)
		{
			if (null != progressDlg && progressDlg.isShowing())
			{
				LogTool.i("ProgressDlg", "hide");
				progressDlg.dismiss();
				progressDlg = null;
				System.gc();
			}
		}
	}

	/**
	 * 子线程中控制ProgressDialog是否显示
	 */
	public static void hidenThreadProgressDlg()
	{

		synchronized (lock)
		{
			// 子线程中去控制进度条的消失
			if (null != progressDlg && progressDlg.isShowing())
			{
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						progressDlg.dismiss();
						progressDlg = null;
					}
				}).start();

				LogTool.i("ProgressDlg", "hide");
				System.gc();
			}
		}
	}

	/**
	 * 创建只有一个按钮的提示框,并显示
	 * 
	 * @param activity
	 * @param message 提示内容
	 * @param title 提示头
	 * @param icon 提示头的图标
	 * @param positiveButtonClickListener
	 */
	public static AlertDialog.Builder createOneButtonDialog(final Activity activity, final String message, final String title, final View view,
			final Drawable icon, final String positiveButtonText, final DialogInterface.OnClickListener positiveButtonClickListener)
	{
		AlertDialog.Builder alertDialog = DialogUtil.createNobuttonDialog(activity, message, title, view, icon);
		String buttonText = null;
		if (ValueUtil.isEmpty(positiveButtonText))
		{
			buttonText = "确定";
		}
		else
		{
			buttonText = positiveButtonText;
		}
		if (positiveButtonClickListener == null)
		{
			alertDialog.setPositiveButton(buttonText, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
				}
			});
		}
		else
		{
			alertDialog.setPositiveButton(buttonText, positiveButtonClickListener);
		}
		return alertDialog;
	}

	/**
	 * 创建具有两个按钮的询问提示框，并显示
	 * 
	 * @param activity
	 * @param message 提示内容
	 * @param title 提示头
	 * @param icon 提示头的图标
	 * @param positiveButtonClickListener 按钮的监听
	 * @param negativeButtonClickListener 按钮的监听
	 */
	public static AlertDialog.Builder createTwoButtonDialog(final Activity activity, final String message, final String title, final View view,
			final Drawable icon, String positiveButtonText, String negativeButtonText, final DialogInterface.OnClickListener positiveButtonClickListener,
			final DialogInterface.OnClickListener negativeButtonClickListener)
	{
		AlertDialog.Builder alertDialog = DialogUtil.createNobuttonDialog(activity, message, title, view, icon);
		String positiveText = null;
		String negativeText = null;
		if (ValueUtil.isEmpty(positiveButtonText))
		{
			positiveText = "确定";
		}
		else
		{
			positiveText = positiveButtonText;
		}
		if (ValueUtil.isEmpty(negativeButtonText))
		{
			negativeText = "取消";
		}
		else
		{
			negativeText = negativeButtonText;
		}
		if (positiveButtonClickListener == null)
		{
			alertDialog.setPositiveButton(positiveText, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
				}
			});
		}
		else
		{
			alertDialog.setPositiveButton(positiveText, positiveButtonClickListener);
		}
		if (negativeButtonClickListener == null)
		{
			alertDialog.setNegativeButton(negativeText, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface arg0, int arg1)
				{
				}

			});
		}
		else
		{
			alertDialog.setNegativeButton(negativeText, negativeButtonClickListener);
		}
		return alertDialog;
	}

	/**
	 * 创建没有button提示框 若view为null则不创建视图 若icon为null则为默认提示头图标
	 * 
	 * @param activity
	 * @param message 提示内容
	 * @param title 提示头
	 * @param view 提示框中增加的视图
	 * @param icon 提示头的图标
	 * @return
	 */
	public static AlertDialog.Builder createNobuttonDialog(final Activity activity, final String message, final String title, View view, Drawable icon)
	{
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		if (title != null)
			alertDialog.setTitle(ValueUtil.isEmpty(title) ? "温馨提示：" : title);
		if (message != null)
			alertDialog.setMessage(message);
		if (view != null)
			alertDialog.setView(view);
		if (icon != null)
			alertDialog.setIcon(icon);
		return alertDialog;
	}

	/**
	 * 创建并显示进度条提示框
	 * 
	 * @param activity
	 * @param message 提示内容
	 * @param title 提示头
	 */
	public static void createProgressDialog(final Activity activity, final String message, final String title)
	{
		if (progressDialog == null)
			progressDialog = new ProgressDialog(activity);
		progressDialog.setTitle(ValueUtil.isEmpty(title) ? "温馨提示：" : title);
		progressDialog.setMessage(ValueUtil.isEmpty(message) ? "正在执行，请稍后..." : message);
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.setCancelable(false); // 不响应返回键
		progressDialog.show();
	}

	public static void createHorizontalProgressDialog(final Activity activity, final int max, final int value, final String title)
	{
		if (horizontalProgressDialog == null)
			horizontalProgressDialog = new ProgressDialog(activity);
		horizontalProgressDialog.setTitle(ValueUtil.isEmpty(title) ? "温馨提示：" : title);
		horizontalProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		horizontalProgressDialog.setIndeterminate(false);
		horizontalProgressDialog.setMax(max);
		horizontalProgressDialog.setProgress(value);
		horizontalProgressDialog.setCanceledOnTouchOutside(false);
		horizontalProgressDialog.setCancelable(false);
		horizontalProgressDialog.show();
	}

	/**
	 * 自定义漂亮的progressDialog
	 * 
	 * @param context
	 * @return
	 */
	public static Dialog createLoadingDialog(Activity activity)
	{

		Dialog dialog = new AlertDialog.Builder(activity).create();
		dialog.setOnCancelListener(new OnCancelListener()
		{
			@Override
			public void onCancel(DialogInterface dialog)
			{
				dialog.cancel();
				// dialog.dismiss();
			}
		});
		dialog.setCanceledOnTouchOutside(false);

		if (!dialog.isShowing())
		{
			dialog.show();
			Window window = dialog.getWindow();
			window.setContentView(R.layout.content_loading_progress);
		}

		return null;
	}

	/**
	 * 取消进度条提示框
	 */
	public static void cancelProgressDialog()
	{
		if (progressDialog != null && progressDialog.isShowing())
		{
			progressDialog.cancel();
			progressDialog = null;
		}
		if (progressDialog != null && progressDialog.isShowing())
		{
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	public static void cancelHorizontalProgressDialog()
	{
		if (horizontalProgressDialog != null && horizontalProgressDialog.isShowing())
		{
			horizontalProgressDialog.cancel();
			horizontalProgressDialog = null;
		}
		if (horizontalProgressDialog != null && horizontalProgressDialog.isShowing())
		{
			horizontalProgressDialog.dismiss();
			horizontalProgressDialog = null;
		}
	}

	/**
	 * 列表对话框，点击一项后触发自定义点击事件。
	 * 
	 * @param context 上下文环境
	 * @param strTitle 对话框标题
	 * @param arrStrItems 选项
	 * @param callBack 选择一项后调用回调方法
	 */
	@SuppressLint("NewApi")
	public static void showEditListDlg(final Activity activity, final String strTitle, final String[] arrStrItems, final IListener<Integer> callBack)
	{

		AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_HOLO_LIGHT);

		builder.setTitle(strTitle);

		builder.setItems(arrStrItems, new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				if (callBack != null)
				{
					callBack.onCall(which);
				}

			}
		});

		builder.show();
	}

	/**
	 * 设置AlertDialog的大小
	 * 
	 * @param dialog
	 * @param width
	 * @param height
	 */
	public static void setParams(AlertDialog dialog, int width, int height)
	{
		WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
		params.width = width;
		params.height = height;
		dialog.getWindow().setAttributes(params);
	}

	/**
	 * 设置dialog Title的颜色
	 * 
	 * @param activity
	 * @param dialog
	 * @param colorResId
	 */
	public static void setWindowStatusBarColor(Activity activity, Dialog dialog, int colorResId)
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			Window window = dialog.getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(dialog.getContext().getResources().getColor(colorResId));
			window.setNavigationBarColor(activity.getResources().getColor(colorResId));
		}
	}
}
