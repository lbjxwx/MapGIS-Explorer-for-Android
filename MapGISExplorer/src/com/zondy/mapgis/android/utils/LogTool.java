package com.zondy.mapgis.android.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import android.util.Log;

/**
 *@author frost
 *@date 20150819
 *@description 日志文件的读写操作,各个系统的日志被自动保存到对应的文件夹下
 */
public class LogTool {
	
	private static Lock lockLog = new ReentrantLock();
	private static ExecutorService exeService = Executors.newSingleThreadExecutor();
	public static final String LOGFOLDERNAME = "Log/LogFiles";
//	public static final String strLogDir= BaseGlobalOper.getCoalPath()+"/"+LOGFOLDERNAME;
	
	/**
	 * 开始一个日志块
	 * @param strMessage
	 */
	public static void startModule(String strMessage){
		strMessage = strMessage == null ? "" : strMessage;
		saveLog("\n\r\n\r" + strMessage + "\n\r");
	}
	/**
	 * 结束一个日志块
	 * @param strMessage
	 */
	public static void endModule(String strMessage){
		strMessage = strMessage == null ? "" : strMessage;
		saveLog("\n\r" + strMessage + "\n\r\n\r");
	}
	
	
	/**
	 * 保存日志，同时在LogCat中打印一条 warn 日志
	 * @param strTag		日志标签
	 * @param strMessage	日志内容
	 */
	public static void w(String strTag, String strMessage){
		saveLog("级别：w " + "，标签：" + strTag + "， 信息：" + strMessage);
		Log.w(strTag, strMessage);
	}
	/**
	 * 保存日志，同时在LogCat中打印一条 error 日志
	 * @param strTag		日志标签
	 * @param strMessage	日志内容
	 */
	public static void e(String strTag, String strMessage){
		saveLog("级别：e " + "， 标签：" + strTag + "， 信息：" + strMessage);
		Log.e(strTag, strMessage);
	}
	/**
	 * 保存日志，同时在LogCat中打印一条 info 日志
	 * @param strTag		日志标签
	 * @param strMessage	日志内容
	 */
	public static void i(String strTag, String strMessage){
		saveLog("级别：i " + "， 标签：" + strTag + "， 信息：" + strMessage);
		Log.i(strTag, strMessage);
	}
	
	/**
	 * 保存日志，同时在LogCat中打印其调用栈
	 * @param e	异常信息
	 */
	public static void exception(Throwable e){
		saveLog("级别：exception " + "， 标签：" + "异常" +  ", 信息： " + StringUtils.getThrowable(e));
		e.printStackTrace();
	}
	
	/**
	 * 保存日志，使用默认路径，每天的日志分开
	 * @param strText
	 */
	public static void saveLog(String strText){
		
		Calendar calendar = Calendar.getInstance(Locale.CHINA);
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.CHINA);
		String strDate = format.format(calendar.getTime());
		
	}
	
	
}
