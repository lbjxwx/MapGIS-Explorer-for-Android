package com.zondy.mapgis.android.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.PaintDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.zondy.mapgis.android.activity.GPSCenterActivity;
import com.zondy.mapgis.android.activity.MapActivity;
import com.zondy.mapgis.android.frag.MeasureAreaFrag;
import com.zondy.mapgis.android.frag.MeasureLengthFrag;
import com.zondy.mapgis.android.frag.RecordTracksFrag;
import com.zondy.mapgis.android.model.EnumViewPos;
import com.zondy.mapgis.explorer.R;

public class PopupWindowUtil
{
	public static PopupWindow popupWindow = null;
	public PopupWindowConfirmListener popupWindowConfirmListener;

	public PopupWindowUtil()
	{
	}

	public PopupWindowUtil(PopupWindowConfirmListener popupWindowConfirmListener)
	{
		this.popupWindowConfirmListener = popupWindowConfirmListener;
	}

	//调用代码备份
//	new PopupWindowUtil(new PopupWindowUtil.PopupWindowConfirmListener()
//	{
//		
//		@Override
//		public void onPopupWindowConfirm(int state, PopupWindow popupWindow)
//		{
//			
//		}
//	}).showToolsPopWindow(this, mMapBottomContent);
	
	/**
	 * 显示工具模块
	 * 
	 * @param context
	 * @param alongParentView
	 */
	public void showToolsPopWindow(final MapActivity mapActivity, View alongParentView)
	{
		View contentView = View.inflate(mapActivity, R.layout.layout_tools_pop, null);

		contentView.findViewById(R.id.gpsstate_btn).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mapActivity.startActivity(new Intent(mapActivity, GPSCenterActivity.class));
			}
		});

		//测量距离
		contentView.findViewById(R.id.measure_length_btn).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				MeasureLengthFrag measureLenFrag = new MeasureLengthFrag();
				mapActivity.mMapBottomContent.setVisibility(View.GONE);
				mapActivity.mMapRightContent.setVisibility(View.GONE);
				mapActivity.addFragment(EnumViewPos.FULL, measureLenFrag, measureLenFrag.getName(), null);
				mapActivity.addFragBackListen(measureLenFrag);
				popupWindow.dismiss();
			}
		});
		// 点击轨迹记录
		contentView.findViewById(R.id.record_tracks_btn).setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				RecordTracksFrag recordTracksFrag = new RecordTracksFrag();
				mapActivity.addFragment(EnumViewPos.FULL, recordTracksFrag, recordTracksFrag.getName(), null);
//				mapActivity.addFragBackListen(recordTracksFrag);
				popupWindow.dismiss();
			}
		});
		//测面积
		contentView.findViewById(R.id.measure_area_btn).setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				MeasureAreaFrag measureAreaFrag = new MeasureAreaFrag();
				mapActivity.addFragment(EnumViewPos.FULL, measureAreaFrag,measureAreaFrag.getName(), null);
				mapActivity.addFragBackListen(measureAreaFrag);
				popupWindow.dismiss();
			}
		});

		popupWindow = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		popupWindow.setWidth(200);
		popupWindow.setHeight(500);
		// 使其聚集
		popupWindow.setFocusable(true);
		// 设置允许在外点击消失
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		// 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景（很神奇的）
		// popupWindow.setBackgroundDrawable(new BitmapDrawable());
		// 指定popWin显示和消失的动画
		popupWindow.setAnimationStyle(R.style.PopWinAnimation);
		popupWindow.showAtLocation(alongParentView, Gravity.BOTTOM, 0, Dp2Px(60));
		// 刷新状态
		popupWindow.update();
	}

	/**
	 * 显示轨迹记录及轨迹参数设置
	 * 
	 * @param mapActivity
	 * @param alongParentView
	 */
	public void showTracksSettingPopWindow(final MapActivity mapActivity, View alongParentView)
	{
		View contentView = View.inflate(mapActivity, R.layout.record_tracks_more_layout, null);
		popupWindow = new PopupWindow(contentView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT,true);
//		setBackgroundAlpha(mapActivity, 0.8f);
//		contentView.setBackgroundColor(mapActivity.getResources().getColor(R.color.alpha));
//		popupWindow.setBackgroundDrawable(mapActivity.getResources().getDrawable(R.drawable.bg_track));
		contentView.findViewById(R.id.mytracks_tv).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(popupWindowConfirmListener != null)
				{
					popupWindowConfirmListener.onPopupWindowConfirm(0,popupWindow);
				}
			}
		});
		contentView.findViewById(R.id.tracks_setting_tv).setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				if(popupWindowConfirmListener != null)
				{
					popupWindowConfirmListener.onPopupWindowConfirm(1,popupWindow);
				}
				
			}
		});
		
		// 使其聚集
		popupWindow.setFocusable(true);
		// 设置允许在外点击消失
		popupWindow.setOutsideTouchable(true);
		 // 如果不设置PopupWindow的背景，无论是点击外部区域还是Back键都无法dismiss弹框
		popupWindow.setBackgroundDrawable(new PaintDrawable());
		popupWindow.setAnimationStyle(R.style.PopWinAnimation);
		ColorDrawable dw = new ColorDrawable(-00000);
        popupWindow.setBackgroundDrawable(dw);
		popupWindow.showAsDropDown(alongParentView);
		// 刷新状态
		popupWindow.update();
	}

	public int Dp2Px(float dp)
	{
		float scale = SysEnv.getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	public PopupWindowConfirmListener getPopupWindowConfirmListener() {
		return popupWindowConfirmListener;
	}

	public void setPopupWindowConfirmListener(
			PopupWindowConfirmListener popupWindowConfirmListener) {
		this.popupWindowConfirmListener = popupWindowConfirmListener;
	}
	
	public interface PopupWindowConfirmListener
	{
		public void onPopupWindowConfirm(int state,PopupWindow popupWindow);
	}

	
	/** 
     * 设置页面的透明度 
     * @param bgAlpha 1表示不透明 
     */  
    public static void setBackgroundAlpha(Activity activity, float bgAlpha) {  
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();  
        lp.alpha = bgAlpha;  
        if (bgAlpha == 1) {  
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug  
        } else {  
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);//此行代码主要是解决在华为手机上半透明效果无效的bug  
        }  
        activity.getWindow().setAttributes(lp);  
    }  


}
