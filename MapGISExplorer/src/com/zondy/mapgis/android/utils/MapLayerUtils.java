package com.zondy.mapgis.android.utils;


import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.core.attr.FieldType;
import com.zondy.mapgis.core.attr.Fields;
import com.zondy.mapgis.core.geodatabase.SFeatureCls;
import com.zondy.mapgis.core.geodatabase.XClsType;
import com.zondy.mapgis.core.map.GroupLayer;
import com.zondy.mapgis.core.map.Map;
import com.zondy.mapgis.core.map.MapLayer;
import com.zondy.mapgis.core.map.ServerLayer;
import com.zondy.mapgis.core.map.VectorLayer;
import com.zondy.mapgis.map.model.LandFieldType;

public class MapLayerUtils
{
	private static String TAG = MapLayerUtils.class.getSimpleName();
	
	/**
	 * 判断当前图层是否是GroupLayer
	 * @param maplayer
	 * @return
	 */
	public static boolean IsGroupLayer(MapLayer maplayer)
	{
		if (!( maplayer instanceof ServerLayer) && maplayer instanceof GroupLayer)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * 控制图层可见不可见
	 * @param maplayer
	 */
	public static void setLayerVisible(MapLayer maplayer)
	{
		//遍历当前图层下的所有图层
		List<MapLayer>  maplayerLst = getAllLayers(maplayer);
		boolean visible = maplayer.getIsVisible();
		for(int i = 0; i < maplayerLst.size();i++)
		{
			maplayerLst.get(i).setVisible(!visible);
		}
		
	}
	
	/**
	 * 获取一级目录下所有图层
	 * @param mapview
	 * @return
	 */
	public static List<MapLayer> getMapviewLayers(MapView mapview)
	{
		List<MapLayer> maplayerLst = new ArrayList<MapLayer>();
		int layercount = mapview.getMap().getLayerCount();
		for(int i = 0; i < layercount;i++)
		{
			maplayerLst.add(mapview.getMap().getLayer(i));
		}
		
		return maplayerLst;
	}
	/**
	 * 获取当前跟图层下面所有子图层（非Grouplayer）
	 * @param rootMapLayer
	 * @return
	 */
	public static List<MapLayer> getAllLayers(MapLayer rootMapLayer)
	{
		List<MapLayer> maplayerLst = new ArrayList<MapLayer>();
		
		if (!(rootMapLayer instanceof ServerLayer) && rootMapLayer instanceof GroupLayer)
		{
			GroupLayer groupLayer1 = (GroupLayer) rootMapLayer;
			// 二级
			for (int b = 0; b < groupLayer1.getCount(); b++)
			{
				MapLayer secondMapLayer = groupLayer1.item(b);
				Log.d(TAG, "secondLayer:" + secondMapLayer.getName());
				maplayerLst.add(secondMapLayer);
				
				// 三级
				if (!(secondMapLayer instanceof ServerLayer) && secondMapLayer instanceof GroupLayer)
				{
					GroupLayer grouplayer2 = (GroupLayer) secondMapLayer;
				
					for(int c = 0;c < grouplayer2.getCount();c++)
					{
						MapLayer threeMaplayer = grouplayer2.item(c);
						Log.e(TAG, "threeMaplayer:" + threeMaplayer.getName());
						maplayerLst.add(threeMaplayer);
						
						//四级
						if(!(threeMaplayer instanceof ServerLayer) && threeMaplayer instanceof GroupLayer)
						{
							GroupLayer grouplayer3 = (GroupLayer) threeMaplayer;
							for(int d = 0;d < grouplayer3.getCount();d++)
							{
								MapLayer fourMaplayer = grouplayer3.item(d);
								Log.d(TAG, "fourMaplayer:" + fourMaplayer.getName());
								
								maplayerLst.add(fourMaplayer);
								//五级
								if(!(fourMaplayer instanceof ServerLayer) && fourMaplayer instanceof GroupLayer)
								{
									GroupLayer grouplayer4 = (GroupLayer) fourMaplayer;
									for(int e = 0; e < grouplayer4.getCount();e++)
									{
										MapLayer fivemapLayer = grouplayer4.item(e);
										Log.e(TAG, "fivemapLayer:" + fivemapLayer.getName());
										maplayerLst.add(fivemapLayer);
									}
								}
								else
								{
									maplayerLst.add(fourMaplayer);
								}
							}
							
						}
						else
						{
							maplayerLst.add(threeMaplayer);
						}
					}
					
				}
				else
				{
					maplayerLst.add(secondMapLayer);
				}
			}

		}
		else
		{
			maplayerLst.add(rootMapLayer);
		}
		
		return maplayerLst;

	}
	
	/**
	 * 获取当前MapView所有图层
	 * @param mapView
	 */
	public static List<MapLayer> getAllMapLayers(MapView mapView)
	{
		List<MapLayer> maplayerLst = new ArrayList<MapLayer>();
		
		int rootLayerCount = mapView.getMap().getLayerCount();
		// 根目录
		for (int rootLayerIndex = 0; rootLayerIndex < rootLayerCount; rootLayerIndex++)
		{
			MapLayer rootMapLayer = mapView.getMap().getLayer(rootLayerIndex);
			Log.e(TAG, "firstLayer==>:" + rootMapLayer.getName());
			maplayerLst.add(rootMapLayer);

			if (!(rootMapLayer instanceof ServerLayer) && rootMapLayer instanceof GroupLayer)
			{
				// 二级
				GroupLayer groupLayer2 = (GroupLayer) rootMapLayer;
				for (int b = 0; b < groupLayer2.getCount(); b++)
				{
					MapLayer secondMapLayer = groupLayer2.item(b);
					Log.d(TAG, "secondLayer==>:" + secondMapLayer.getName());
					Log.d(TAG, "rootLayerIndex==>:" + rootLayerIndex);
					maplayerLst.add(secondMapLayer);

					if (!(secondMapLayer instanceof ServerLayer) && secondMapLayer instanceof GroupLayer)
					{
						// 三级
						GroupLayer groupLayer3 = (GroupLayer) secondMapLayer;

						for (int c = 0; c < groupLayer3.getCount(); c++)
						{
							MapLayer threeMapLayer = groupLayer3.item(c);
							Log.e(TAG, "threeMapLayer:" + threeMapLayer.getName());
							Log.e(TAG, "b==>:" + b);
							maplayerLst.add(threeMapLayer);
							// 四级
							if (!(threeMapLayer instanceof ServerLayer) && threeMapLayer instanceof GroupLayer)
							{
								GroupLayer groupLayer4 = (GroupLayer) threeMapLayer;
								for (int d = 0; d < groupLayer4.getCount(); d++)
								{
									MapLayer fourMaplayer = groupLayer4.item(d);
									Log.d(TAG, "fourMaplayer==>:" + fourMaplayer.getName());
									maplayerLst.add(fourMaplayer);
								}
							}

						}
					}
					
					if(secondMapLayer instanceof ServerLayer)
					{
						// 服务三级
						GroupLayer groupLayer3 = (GroupLayer) secondMapLayer;

						for (int c = 0; c < groupLayer3.getCount(); c++)
						{
							MapLayer threeMapLayer = groupLayer3.item(c);
							Log.e(TAG, "服务threeMapLayer:" + threeMapLayer.getName());
							maplayerLst.add(threeMapLayer);
						}
					}

				}
			}
			//服务图层
			if(rootMapLayer instanceof ServerLayer)
			{
				// 服务图层二级
				GroupLayer groupLayer2 = (GroupLayer) rootMapLayer;
				for (int b = 0; b < groupLayer2.getCount(); b++)
				{
					MapLayer secondMapLayer = groupLayer2.item(b);
					Log.d(TAG, "服务secondLayer==>:" + secondMapLayer.getName());
					maplayerLst.add(secondMapLayer);
				}
			}
		}
		return maplayerLst;
	}
	
	/**
	 * 获取当前Map所有矢量图层
	 * @param mapView
	 * @return
	 */
	public static List<MapLayer> getAllVectorLayer(MapView mapView)
	{
		List<MapLayer> allMapLayerLst = getAllMapLayers(mapView);
		List<MapLayer> vectorLayerLst = new ArrayList<MapLayer>();
		
		for(int i =0; i < allMapLayerLst.size();i++)
		{
			if(allMapLayerLst.get(i) instanceof VectorLayer)
			{
				vectorLayerLst.add(allMapLayerLst.get(i));
			}
		}
		
		return vectorLayerLst;
	}
	
	/**
	 * 根据地图获取所有可见图层
	 * @param map
	 * @return 返回由列表索引0对应地图最低层图层，索引最大值对应地图最上层的图层
	 */
	public static List<MapLayer> getVisibleLayers(Map map) {
		List<MapLayer> maplayerLst = getAllLayers(map);
		if(maplayerLst == null || maplayerLst.size() < 1) return null;
		for (int i = maplayerLst.size() -1; i >= 0; i--) {
			MapLayer layer =  maplayerLst.get(i);
			if (!layer.getIsValid() || !layer.getIsVisible()) {
				maplayerLst.remove(i);
			}
		}
		return maplayerLst;
	}
	
	/**
	 * 根据地图获取所有图层
	 * @param map 地图
	 * @return 返回由列表索引0对应地图最低层图层，索引最大值对应地图最上层的图层
	 */
	public static List<MapLayer> getAllLayers(Map map) {
		List<MapLayer> maplayerLst = new ArrayList<MapLayer>();
		int layerConut1 = map.getLayerCount();
		if(layerConut1 < 1) return null;
		for (int i = 0; i < layerConut1; i++) {
			MapLayer layer = map.getLayer(i);
			getVisibleLayer(layer, maplayerLst);
		}
		return maplayerLst;
	}
	
	private static void getVisibleLayer(MapLayer MapLayer, List<MapLayer> maplayerLst) {
		// TODO: 判断是否有误，待确认
		if (!(MapLayer instanceof ServerLayer) && MapLayer instanceof GroupLayer) {
			GroupLayer grouplayer = (GroupLayer) MapLayer;
			for (int i = 0; i < grouplayer.getCount(); i++) {
				MapLayer subMapLayer = grouplayer.item(i);
				getVisibleLayer(subMapLayer, maplayerLst);
			}
		} else {
			maplayerLst.add(MapLayer);
		}

	}
	

	
	/**
	 * 将图层的所有字段名和字段类型存在输入参数中，输入参数应该是不为null的空队列
	 * @param maplayer
	 * @param fldNames
	 * @param fldTyps
	 * @return
	 */
	public static int getFields(MapLayer mapLayer,List<String> fldNames, List<LandFieldType> fldTyps) {

		if (fldNames == null || fldTyps == null) {
			throw new IllegalArgumentException(
					"Layer.getFields, 输入参数不正确。 fldNames == null || fldTypes == null");
		}
		if (mapLayer == null) {
			System.out.println("当前图层为空或者不是矢量图层，无法处理，直接返回");
			return 0;
		}
		if (!(mapLayer instanceof VectorLayer)) {
			return 1;
		}
		if (mapLayer.getData() == null) {
			return 2;
		}
		if (mapLayer.getData().getClsType() == XClsType.SFCls) {
			SFeatureCls cls = (SFeatureCls) mapLayer.getData();

			Fields fields = cls.getFields();
			int fieldCount = fields.getFieldCount();
			for (short ii = 0; ii < fieldCount; ii++) {

				String strName = fields.getField(ii).getFieldName();
				FieldType fldType = fields.getField(ii).getFieldType();
				fldNames.add(strName);
				fldTyps.add(LandFieldType.fromFieldType(fldType));
			}
			return 3;
		} else {
			System.out.println("当前图层不是简单要素类图层");
			return 4;
		}
	}
}
