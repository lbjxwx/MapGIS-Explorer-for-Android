package com.zondy.mapgis.android.map.dao;

import java.util.ArrayList;
import java.util.List;

import com.zondy.mapgis.core.srs.SRefData;
import com.zondy.mapgis.map.model.DRect;
import com.zondy.mapgis.map.model.LandFieldType;

/**
 * @content 底图图层基础接口，把底图图层抽取接口出来，可以方便地以相同方法处理 mapgis 和 arcgis 的图层类
 * @author admin 2016-8-1 下午4:39:56
 */
public interface IBaseLayer {

	/**
	 * 是否组图层
	 * @return
	 */
	public boolean isGroupLayer();
	/**
	 * 是否矢量图层
	 * @return
	 */
	public boolean isVectorLayer();

	/**
	 * 图层名
	 * @return
	 */
	public String getName();
	/**
	 * 设置子图层
	 */
	public void setChild(ArrayList<IBaseLayer> childLyrLst);

	/**
	 * 图层别名
	 * @return
	 */
	public String getAlias();

	/**
	 * 设置图层别名
	 * @param alias
	 */
	public void setAlias(String alias);

	/**
	 * 如果有别名，则取别名，否则，取图层名
	 * @return
	 */
	public String getAliasOrName();

	/**
	 * 是否选中图层，这个方法应该弃用。
	 * @return
	 */
	public boolean isDefSelect();

	/**
	 * 设置是否选中图层，这个方法应该弃用
	 * @param defSelect
	 */
	public void setDefSelect(boolean defSelect);

	/**
	 * 设置图层可见性
	 * @param visible
	 */
	public void setVisible(boolean visible);

	/**
	 * 图层是否可用
	 * @return
	 */
	public boolean isVisible();

	/**
	 * 图层是否可编辑
	 * @return
	 */
	public boolean isEditable();

	/**
	 * 设置图层是否可编辑
	 * @param isEditable
	 */
	public void setEditable(boolean isEditable);

	/**
	 * 设置图层是否可应用透明度
	 * @return
	 */
	public boolean hasTransparency();

	/**
	 * 取图层透明度
	 * @return
	 */
	public int getTransparency();

	/**
	 * 设置图层透明度
	 * @param transparency
	 * @return
	 */
	public boolean setTransparency(int transparency);

	/**
	 * 图层所有子图层
	 * @return
	 */
	public ArrayList<IBaseLayer> getChildLayers();

	/**
	 * 所有子图层名
	 * @return
	 */
	public ArrayList<String> getChildLayerNames();

	/**
	 * 指定名称对应的子图层
	 * @param strName
	 * @return
	 */
	public IBaseLayer getChildLayerByName(String strName);

	/**
	 * 计算图层简要信息
	 * @return
	 */
//	public LayerInfoMin getLayerInfoMin();

	/**
	 * 应用图层简要信息
	 * @param lim
	 */
//	public void applyInfo(LayerInfoMin lim);

	/**
	 * 取坐标系
	 * @return
	 */
	public SRefData getSpatialReference();
	
	/**
	 * 图层范围
	 * @return
	 */
	public DRect getRange();

	/**
	 * 设置最大放大级数
	 * @return
	 */
	public double getMaxScale();
	/**
	 * 设置最小放大级数
	 * @return
	 */
	public double getMinScale();
	
	/**
	 * 将图层字段和字段类型放入参数中
	 * @param fldNames
	 * @param fldTyps
	 * @return 
	 */
	public int getFields(List<String> fldNames, List<LandFieldType> fldTyps);
}
