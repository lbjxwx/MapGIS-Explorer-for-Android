package com.zondy.mapgis.android.map.dao;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.view.View;

/**
 * Fragment接口
 * @author 曾繁添
 * @version 1.0
 *
 */
public interface IBaseFragment extends IGoBack {

	/**
	 * 绑定渲染视图的布局文件
	 * @return 布局文件资源id
	 */
	public int bindLayout();
	
	/**
	 * 初始化控件
	 */
	public void initView(final View view);
	
	/**
	 * 业务处理操作（onCreateView方法中调用）
	 * @param mContext  当前Activity对象
	 */
	public void doBusiness(Context mContext);
	
	/**
	 * 返回上一步<br>
	 * 当前{@link Fragment}处理自身的{@link goback()}事件，如果成功处理完毕，则返回true，如果没有处理完成，则返回false，上一级可以根据其返回值
	 * 确定下一步操作。
	 * @return
	 */
	@Override
	public abstract boolean goback();
	/**
	 * 是否固定在所在{@link Activity}中
	 * @return
	 */
	public abstract boolean isLocked();
	/**
	 * 返回{@link BaseFragment}的名称
	 * @return
	 */
	public abstract String getName();
	
}
