package com.zondy.mapgis.android.map.dao;


/**
 * Activity接口
 * @author FJL
 * @version 1.0
 *
 */
public interface IBaseActivity {

	
	
	/**
	 * 暂停恢复刷新相关操作（onResume方法中调用）
	 */
	public void resume();
	
	/**
	 * 销毁、释放资源相关操作（onDestroy方法中调用）
	 */
	public void destroy();
	
	/**
	 * 终止Activity
	 */
	public void finishActivity();
	
}
