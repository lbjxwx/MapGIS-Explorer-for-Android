package com.zondy.treebean;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.zondy.mapgis.android.mapview.MapView;
import com.zondy.mapgis.android.utils.MapLayerUtils;
import com.zondy.mapgis.android.utils.MapUtil;
import com.zondy.mapgis.core.geometry.GeomType;
import com.zondy.mapgis.core.map.GroupLayer;
import com.zondy.mapgis.core.map.ServerLayer;
import com.zondy.mapgis.core.object.Enumeration;
import com.zondy.mapgis.explorer.R;

/**
 * @author fjl 2016-11-1 下午3:27:24 * @param <T>
 */
public abstract class TreeListViewAdapter<T> extends BaseAdapter
{

	protected Context mContext;
	private MapView mMapView; 
	/**
	 * 存储所有可见的Node
	 */
	protected List<Node> mNodes;
	protected LayoutInflater mInflater;
	/**
	 * 存储所有的Node
	 */
	protected List<Node> mAllNodes;

	/**
	 * 点击的回调接口
	 */
	private OnTreeNodeClickListener onTreeNodeClickListener;

	public interface OnTreeNodeClickListener
	{
		void onClick(Node node, int position);
	}

	public void setOnTreeNodeClickListener(OnTreeNodeClickListener onTreeNodeClickListener)
	{
		this.onTreeNodeClickListener = onTreeNodeClickListener;
	}

	/**
	 * 
	 * @param mTree
	 * @param context
	 * @param datas
	 * @param defaultExpandLevel 默认展开几级树
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public TreeListViewAdapter(ListView mTree, Context context, List<T> datas, int defaultExpandLevel,MapView mapview) throws IllegalArgumentException, IllegalAccessException
	{
		mContext = context;
		mMapView = mapview;
		/**
		 * 对所有的Node进行排序
		 */
		mAllNodes = TreeHelper.getSortedNodes(datas, defaultExpandLevel);
		/**
		 * 过滤出可见的Node
		 */
		mNodes = TreeHelper.filterVisibleNode(mAllNodes);
		mInflater = LayoutInflater.from(context);

		/**
		 * 设置节点点击时，可以展开以及关闭；并且将ItemClick事件继续往外公布
		 */
		mTree.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				expandOrCollapse(position);

				if (onTreeNodeClickListener != null)
				{
					onTreeNodeClickListener.onClick(mNodes.get(position), position);
				}
			}

		});

	}

	/**
	 * 相应ListView的点击事件 展开或关闭某节点
	 * 
	 * @param position
	 */
	public void expandOrCollapse(int position)
	{
		Node n = mNodes.get(position);

		if (n != null)// 排除传入参数错误异常
		{
			if (!n.isLeaf())
			{
				n.setExpand(!n.isExpand());
				mNodes = TreeHelper.filterVisibleNode(mAllNodes);
				notifyDataSetChanged();// 刷新视图
			}
		}
	}

	@Override
	public int getCount()
	{
		return mNodes.size();
	}

	@Override
	public Object getItem(int position)
	{
		return mNodes.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final Node node = mNodes.get(position);

		ViewHolder viewHolder = null;
		convertView = getConvertView(node, position, convertView, parent);
		if (convertView == null)
		{
			convertView = mInflater.inflate(R.layout.mapmanager_list_item, parent, false);
			// 设置内边距
			convertView.setPadding(node.getLevel() * 30, 3, 3, 3);

			viewHolder = new ViewHolder();
			viewHolder.icon = (ImageView) convertView.findViewById(R.id.id_treenode_icon);
			viewHolder.imgView = (ImageView) convertView.findViewById(R.id.maplayer_img_iv);
			viewHolder.nameText = (TextView) convertView.findViewById(R.id.maplayer_name_tv);
			viewHolder.valueText = (TextView) convertView.findViewById(R.id.common_fieldvalue);
			viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.maplayer_select_check);

			// here
			viewHolder.checkBox.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					CheckBox check = (CheckBox) v;
					boolean visible = check.isChecked();
					node.getMaplayer().setVisible(visible);
					MapUtil.updateMap(mMapView);
				}
			});

			convertView.setTag(viewHolder);

		}
		else
		{
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// ico
		if (node.getIcon() == -1)
		{
			viewHolder.icon.setVisibility(View.INVISIBLE);
		}
		else
		{
			viewHolder.icon.setVisibility(View.VISIBLE);
			viewHolder.icon.setImageResource(node.getIcon());
			notifyDataSetChanged();
		}

		// checkbox
		if (MapLayerUtils.IsGroupLayer(node.getMaplayer()))
		{
			viewHolder.checkBox.setVisibility(View.GONE);
		}
		else
		{
			viewHolder.checkBox.setVisibility(View.VISIBLE);
		}
		if (node.getMaplayer().getIsVisible())
		{
			viewHolder.checkBox.setChecked(true);
		}
		else
		{
			viewHolder.checkBox.setChecked(false);
		}
		Enumeration en = node.getMaplayer().GetGeometryType().parse(GeomType.class, 1);
		int geometryType = node.getMaplayer().GetGeometryType().value();
		// MapLayer img
		if (!(node.getMaplayer() instanceof ServerLayer) && node.getMaplayer() instanceof GroupLayer)
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.file_icon);
			node.getMaplayer().setVisible(true);
		}
		else if (1 == geometryType)
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.icon_lyr_pnt);
		}
		else if (2 == geometryType)
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.icon_lyr_lin);
		}
		else if (3 == geometryType)
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.icon_lyr_reg);
		}
		else
		{
			viewHolder.imgView.setBackgroundResource(R.drawable.layer_icon);
		}

		// name
		viewHolder.nameText.setText(node.getName());

		return convertView;
	}

	public abstract View getConvertView(Node node, int position, View convertView, ViewGroup parent);

	private class ViewHolder
	{
		public ImageView icon;
		public ImageView imgView;
		public TextView nameText;
		public TextView valueText;
		public CheckBox checkBox;
	}

}
