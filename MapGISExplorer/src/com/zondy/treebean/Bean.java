package com.zondy.treebean;

import java.io.Serializable;

import com.zondy.mapgis.core.map.MapLayer;


public class Bean implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@TreeNodeId
	private int id;
	@TreeNodePid
	private int pId;
	@TreeNodeLabel
	private String label;
	
	private MapLayer mapLayer;
	


	public Bean()
	{
	}

	public Bean(int id, int pId, String label,MapLayer mapLayer)
	{
		this.id = id;
		this.pId = pId;
		this.label = label;
		this.mapLayer = mapLayer;
	}
	
	public Bean(int id, int pId, String label)
	{
		this.id = id;
		this.pId = pId;
		this.label = label;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getpId()
	{
		return pId;
	}

	public void setpId(int pId)
	{
		this.pId = pId;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}
	
	public MapLayer getMapLayer()
	{
		return mapLayer;
	}

	public void setMapLayer(MapLayer mapLayer)
	{
		this.mapLayer = mapLayer;
	}

}
