#MapGIS Explorer for Android

#一、简介

![](http://git.oschina.net/uploads/images/2017/0116/173403_aad5d237_1195689.png "在这里输入图片标题")



MapGIS Explorer提供的功能如下：

- 地图浏览功能：支持用户选取地图文档、输入在线地图服务地址、选择常用免费地图服务如百度、OSM等浏览地图。

- 量算功能：距离测量，面积测量

- 定位功能：通过百度定位接口实现定位，支持GNSS、基站、WiFi定位。

- 轨迹记录功能：可将用户行进的轨迹以GPX格式保存到SD卡中，支持后台运行。提供轨迹文件的管理功能，支持轨迹在地图上的叠加显示，能够显示轨迹的记录时间和总长度

- 查询功能：对矢量数据的属性查询和空间查询功能，支持查询结果的高亮显示、标注和列表显示，支持滑动分页，点击可查看详情。

- 定位数据转换：设置三参数、七参数

## 二、配置开发环境

>配置开发环境前请到云开发世界下载MapGIS_Mobile_Android_SDK，下载的MapGIS_Mobile_Android_SDK .zip 解压后里面有示例demo，开发文档及开发所需要的Jar包和So库，下载位置如下图所示：

![输入图片说明](http://git.oschina.net/uploads/images/2017/0117/090845_bbef011d_1195689.png "在这里输入图片标题")

> 开发工具

Android开发工具很多，在这我们推荐各位开发者使用Eclipse和Android Studio作为自己的开发工具。








## 三、授权

> 第一步：进入官网[注册开发者账号](http://www.smaryun.com/)

![输入图片说明](http://git.oschina.net/uploads/images/2017/0117/091113_0d964c86_1195689.png "在这里输入图片标题")

>第二步：登录注册的开发者账号获取开发授权

<div align="center">
<img src="http://git.oschina.net/uploads/images/2017/0117/092112_1712ee47_1195689.png" width = "360" height = "640" alt="图片名称" align=center />
</div>









# 附录1：软件截图
### :MapGIS Explorer: Android和iOS运行效果：

<div align="center">
<img src="http://git.oschina.net/uploads/images/2017/0117/094230_2af9a640_1195689.png" width = "360" height = "640" alt="图片名称" align=center />
</div>


<div align="center">
<img src="http://git.oschina.net/uploads/images/2017/0117/092353_ef881755_1195689.png" width = "360" height = "640" alt="图片名称" align=center />
</div>

### :查询运行效果

<div align="center">
<img src="http://git.oschina.net/uploads/images/2017/0117/093117_d4d3df4e_1195689.png" width = "360" height = "640" alt="图片名称" align=center />
</div>

### :工具运行效果
<div align="center">
<img src="http://git.oschina.net/uploads/images/2017/0117/094046_f3b5a589_1195689.png" width = "360" height = "640" alt="图片名称" align=center />
</div>
### :更多运行效果


<div align="center">
<img src="http://git.oschina.net/uploads/images/2017/0117/093432_ca6e0e6c_1195689.png" width = "360" height = "640" alt="图片名称" align=center />
</div>

